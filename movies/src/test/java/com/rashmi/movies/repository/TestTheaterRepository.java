package com.rashmi.movies.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.rashmi.movies.entity.Movie;
import com.rashmi.movies.entity.Theater;
import com.rashmi.movies.entity.impl.MovieImpl;
import com.rashmi.movies.entity.impl.TheaterImpl;

@ContextConfiguration(locations = { "classpath:spring-context.xml" })
public class TestTheaterRepository
        extends AbstractTransactionalJUnit4SpringContextTests {
    @Autowired
    private TheaterRepository theaterRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Rollback(false)
    @Test
    public void addAndGetTheater() {
        Theater newTheater = createTheater();

        long addedTheaterId = theaterRepository.addTheater(newTheater);
        System.out.println("Theater added id " + addedTheaterId);
        Assert.assertNotEquals(0, addedTheaterId);

        Theater found = theaterRepository
                .getTheater(addedTheaterId);
        Assert.assertEquals(found.getTheaterId(), addedTheaterId);
        Assert.assertEquals(found.getTheaterName(addedTheaterId),
                newTheater.getTheaterName(newTheater.getTheaterId()));
    }

    @Rollback(false)
    @Test
    public void addAndGetTheaterList() {
        Theater theater1 = createTheater();

        Movie movie1 = createMovie();

        List<Theater> tList = new ArrayList<Theater>();

        List<Movie> mList = new ArrayList<Movie>();

        tList.add(theater1);
        mList.add(movie1);

        movie1.setTheaters(tList);
        theater1.setMovies(mList);

        long addedTheaterId = theaterRepository.addTheater(theater1);
        long addedMovie = movieRepository.addMovie(movie1);

        System.out.println("Theater added id " + addedTheaterId);
        Assert.assertNotEquals(0, addedTheaterId);

        Theater foundTheater = theaterRepository.getTheater(addedTheaterId);
        Assert.assertEquals(foundTheater.getTheaterId(), addedTheaterId);
        Assert.assertEquals(foundTheater.getTheaterName(addedTheaterId),
                theater1.getTheaterName(addedTheaterId));

        System.out.println("Movie added id " + addedMovie);
        Assert.assertNotEquals(0, addedMovie);

        Movie foundMovie = movieRepository.getMovie(addedMovie);
        Assert.assertEquals(foundMovie.getMovieId(), addedMovie);
        Assert.assertEquals(foundMovie.getMovieName(addedMovie),
                movie1.getMovieName(addedMovie));
    }

    private Movie createMovie() {
        MovieImpl newMovie = new MovieImpl();
        long rand = new Random().nextInt(99999);

        newMovie.setMovieName("movie-Repository" + rand);
        newMovie.setMovieDetails("this is movie details");

        return newMovie;
    }

    private Theater createTheater() {
        TheaterImpl newTheater = new TheaterImpl();
        long rand = new Random().nextInt(99999);

        newTheater.setTheaterName("theater-Repository" + rand);
        newTheater.setPhone("4087221060");
        newTheater.setTheaterAminities("Cool Amenities");
        newTheater.setTheaterCity("SanJose");
        newTheater.setTheaterState("CA");
        newTheater.setTheaterZip(95117);

        return newTheater;
    }
}