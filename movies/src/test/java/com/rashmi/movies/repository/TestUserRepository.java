package com.rashmi.movies.repository;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import com.rashmi.movies.entity.Address;
import com.rashmi.movies.entity.User;
import com.rashmi.movies.entity.impl.AddressImpl;
import com.rashmi.movies.entity.impl.UserImpl;
import com.rashmi.movies.service.exception.MovieException;

@ContextConfiguration(locations = { "classpath:spring-context.xml" })
public class TestUserRepository
        extends AbstractTransactionalJUnit4SpringContextTests {
    @Autowired
    private UserRepository userRepository;

    @Rollback(false)
    @Test
    public void addAndGetUser() {
        User newUser = createUser();

        long addedUserId = 0;
        try {
            addedUserId = userRepository.addUser(newUser);
        } catch (MySQLIntegrityConstraintViolationException
                | MovieException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("user added id " + addedUserId);
        Assert.assertNotEquals(0, addedUserId);

        User found = userRepository.getUser(addedUserId);
        Assert.assertEquals(found.getUserId(), addedUserId);
        Assert.assertEquals(found.getFirstName(), newUser.getFirstName());
        Assert.assertEquals(found.getLastName(), newUser.getLastName());
        Assert.assertEquals(found.getPassword(), newUser.getPassword());
    }

    private User createUser() {
        UserImpl newUser = new UserImpl();
        long rand = new Random().nextInt(99999);

        Address newAddress = createAddress();

        newUser.setFirstName("firstname" + rand);
        newUser.setLastName("lastName-Repository");
        newUser.setEmailId("email" + rand + "@test.com");
        newUser.setPassword("test");
        newUser.setPaymentType("credit card");
        newUser.setPhone(1234);
        // Adding address to both sides
        newUser.setAddress(newAddress);
        newAddress.setUser(newUser);

        return newUser;
    }

    private Address createAddress() {
        AddressImpl newAddress = new AddressImpl();

        newAddress.setStreet1("2530");
        newAddress.setStreet2("Fernwood Ave");
        newAddress.setCity("SanJose");
        newAddress.setState("CA");
        newAddress.setZip(95117);
        newAddress.setCountry("Repository-USA");

        return newAddress;
    }
}