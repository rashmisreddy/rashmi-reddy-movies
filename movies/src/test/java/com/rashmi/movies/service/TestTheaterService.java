package com.rashmi.movies.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.rashmi.movies.entity.Movie;
import com.rashmi.movies.entity.Theater;
import com.rashmi.movies.entity.impl.MovieImpl;
import com.rashmi.movies.entity.impl.TheaterImpl;

@ContextConfiguration(locations = { "classpath:spring-context.xml" })
public class TestTheaterService extends AbstractJUnit4SpringContextTests {
    @Autowired
    private TheaterService theaterService;

    @Autowired
    private MovieService movieService;

    @Rollback(false)
    @Test
    public void addAndGetTheaterList() {
        Theater theater1 = createTheater();

        Movie movie1 = createMovie();

        List<Theater> tList = new ArrayList<Theater>();

        List<Movie> mList = new ArrayList<Movie>();

        tList.add(theater1);
        mList.add(movie1);

        movie1.setTheaters(tList);
        theater1.setMovies(mList);

        Theater addedTheater = theaterService.addTheater(theater1);
        Movie addedMovie = movieService.addMovie(movie1);

        System.out.println("Theater added id " + addedTheater.getTheaterId());
        Assert.assertNotEquals(0, addedTheater.getTheaterId());

        Theater foundTheater = theaterService
                .getTheater(addedTheater.getTheaterId());
        Assert.assertEquals(foundTheater.getTheaterId(),
                addedTheater.getTheaterId());
        Assert.assertEquals(
                foundTheater.getTheaterName(addedTheater.getTheaterId()),
                theater1.getTheaterName(addedTheater.getTheaterId()));

        System.out.println("Movie added id " + addedMovie.getMovieId());
        Assert.assertNotEquals(0, addedMovie.getMovieId());

        Movie foundMovie = movieService.getMovie(addedMovie.getMovieId());
        Assert.assertEquals(foundMovie.getMovieId(), addedMovie.getMovieId());
        Assert.assertEquals(foundMovie.getMovieName(addedMovie.getMovieId()),
                movie1.getMovieName(addedMovie.getMovieId()));
    }

    private Movie createMovie() {
        MovieImpl newMovie = new MovieImpl();
        long rand = new Random().nextInt(99999);

        newMovie.setMovieName("movie-Service" + rand);
        newMovie.setMovieDetails("this is movie details");

        return newMovie;
    }

    private Theater createTheater() {
        TheaterImpl newTheater = new TheaterImpl();
        long rand = new Random().nextInt(99999);

        newTheater.setTheaterName("theater-Service" + rand);
        newTheater.setPhone("4087221060");
        newTheater.setTheaterAminities("Cool Amenities");
        newTheater.setTheaterCity("SanJose");
        newTheater.setTheaterState("CA");
        newTheater.setTheaterZip(95117);

        return newTheater;
    }
}