package com.rashmi.movies.service;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.rashmi.movies.entity.Address;
import com.rashmi.movies.entity.User;
import com.rashmi.movies.entity.impl.AddressImpl;
import com.rashmi.movies.entity.impl.UserImpl;

@ContextConfiguration(locations = { "classpath:spring-context.xml" })
public class TestUserService extends AbstractJUnit4SpringContextTests {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserService userService;

    @Test
    public void testPassword() {
        User newUser = createUser();

        User added = userService.addUser(newUser);
        User checkUser = userService.getUser(added.getUserId());
        Assert.assertEquals(added.getPassword(), checkUser.getPassword());
        Assert.assertEquals(true, userService.isValidPassword(added.getUserId(),
                newUser.getEmailId(), "test"));
    }

    @Test
    public void addAndGetUser() {
        User newUser = createUser();

        User added = userService.addUser(newUser);
        logger.info("user added " + added);
        Assert.assertNotEquals(0, added.getUserId());// this should have been
        // created so not zero anymore
        Assert.assertEquals(newUser.getFirstName(), added.getFirstName());
        Assert.assertEquals(newUser.getLastName(), added.getLastName());
        Assert.assertEquals(newUser.getPassword(), added.getPassword());

        User found = userService.getUser(added.getUserId());
        Assert.assertEquals(found.getUserId(), added.getUserId());
        Assert.assertEquals(found.getFirstName(), added.getFirstName());
        Assert.assertEquals(found.getLastName(), added.getLastName());
        Assert.assertEquals(found.getEmailId(), added.getEmailId());
        Assert.assertEquals(found.getPassword(), added.getPassword());
        Assert.assertEquals(found.getPaymentType(), added.getPaymentType());
        Assert.assertEquals(found.getPhone(), added.getPhone());

        Assert.assertEquals(found.getPassword(), added.getPassword());
    }
    
    
    private User createUser() {
        UserImpl newUser = new UserImpl();
        long rand = new Random().nextInt(99999);

        Address newAddress = createAddress();
        
        newUser.setFirstName("firstname" + rand);
        newUser.setLastName("lastName-Service");
        newUser.setEmailId("email" + rand + "@test.com");
        newUser.setPassword("test");
        newUser.setPaymentType("credit card");
        newUser.setPhone(1234);
        // Adding address to both sides
        newUser.setAddress(newAddress);
        newAddress.setUser(newUser);

        return newUser;
    }
    
    private Address createAddress() {
        AddressImpl newAddress = new AddressImpl();

        newAddress.setStreet1("2530");
        newAddress.setStreet2("Fernwood Ave");
        newAddress.setCity("SanJose");
        newAddress.setState("CA");
        newAddress.setZip(95117);
        newAddress.setCountry("USA-Service");

        return newAddress;
    }
}
