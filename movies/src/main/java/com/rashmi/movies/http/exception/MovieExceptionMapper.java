package com.rashmi.movies.http.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Component;

import com.rashmi.movies.service.exception.MovieException;

/**
 * Return HTTP 409 with response body
 * 
 */
@Provider
@Component
public class MovieExceptionMapper implements ExceptionMapper<MovieException> {

    @Override
    public Response toResponse(MovieException ex) {
        return Response.status(Status.CONFLICT).entity(new HttpError(ex))
                .build();
    }

}
