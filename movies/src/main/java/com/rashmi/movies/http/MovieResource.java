package com.rashmi.movies.http;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rashmi.movies.entity.Movie;
import com.rashmi.movies.entity.impl.MovieImpl;
import com.rashmi.movies.http.entity.HttpMovie;
import com.rashmi.movies.service.MovieService;
import com.rashmi.movies.service.exception.MovieException;

@Path("/movies")
// @ApplicationPath("/movies")
@Component
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public class MovieResource extends Application {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MovieService movieService;

    public enum ErrorCode {
        INVALID_FIELD, MISSING_DATA
    }

    @POST
    @Path("/")
    public Response createMovie(HttpMovie newMovie) throws MovieException {
        Movie movieToCreate = convert(newMovie);
        Movie addedMovie = movieService.addMovie(movieToCreate);
        return Response.status(Status.CREATED)
                .header("Location", "/movies/" + addedMovie.getMovieId())
                .entity(new HttpMovie(addedMovie)).build();
    }

    @DELETE
    @Path("/delete/{movieId}")
    public Response deleteMovie(@PathParam("movieId") long movieId,
            HttpMovie newMovie) throws MovieException {
        Movie deleteMovie = movieService.deleteMovie(movieId);

        return Response.status(Status.OK)
                .header("Location", "/movie/" + deleteMovie.getMovieId())
                .entity(new HttpMovie(deleteMovie)).build();
    }

    @PUT
    @Path("/update")
    public Response updateMovie(@QueryParam("movieId") long movieId,
            HttpMovie updateMovie) throws MovieException {
        Movie movieToUpdate = convert(updateMovie);
        movieService.updateMovie(movieToUpdate, movieId);
        Movie updatedMovie = movieService.getMovie(movieId);
        return Response.status(Status.OK)
                .header("Location", "/movie/" + movieId)
                .entity(new HttpMovie(updatedMovie)).build();
    }

    @GET
    @Path("/{movieId}")
    public HttpMovie getMovieById(@PathParam("movieId") long movieId)
            throws MovieException {
        logger.info("getting movie by id:" + movieId);
        Movie movie = movieService.getMovie(movieId);
        if (movie != null)
            return new HttpMovie(movie);
        else
            return null;
    }

    @GET
    @Path("/all")
    public List<HttpMovie> getMovies() throws MovieException {
        logger.info("getting all movies");
        List<Movie> movie = movieService.getAllMovies();

        if (movie != null) {
            List<HttpMovie> httpMovie = new ArrayList<HttpMovie>();

            for (Movie m : movie) {
                httpMovie.add(new HttpMovie(m));
            }
            return httpMovie;
        } else
            return null;
    }

    @GET
    @Path("/")
    @Wrapped(element = "movie")
    public List<HttpMovie> getMovieSearch(
            @QueryParam("movieName") String movieName) throws MovieException {
        List<HttpMovie> returnList = new ArrayList<>();

        logger.info("movieName search =" + movieName);
        List<Movie> found = movieService.searchMovies(movieName);
        if (found != null) {
            for (Movie movie : found) {
                returnList.add(new HttpMovie(movie));
            }
            return returnList;
        } else {
            return null;
        }
    }

    private Movie convert(HttpMovie httpMovie) {
        if (httpMovie == null) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "Movie object is not provided in body");
        }

        MovieImpl newMovie = new MovieImpl();

        newMovie.setMovieName(httpMovie.movieName);
        newMovie.setMovieDetails(httpMovie.movieDetails);

        return newMovie;
    }
}
