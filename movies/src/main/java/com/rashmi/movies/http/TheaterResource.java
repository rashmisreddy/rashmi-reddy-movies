package com.rashmi.movies.http;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rashmi.movies.entity.Theater;
import com.rashmi.movies.entity.impl.TheaterImpl;
import com.rashmi.movies.http.entity.HttpTheater;
import com.rashmi.movies.service.TheaterService;
import com.rashmi.movies.service.exception.MovieException;

@Path("/theaters")
@ApplicationPath("/theaters")
@Component
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public class TheaterResource extends Application {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TheaterService theaterService;

    @POST
    @Path("/")
    public Response createTheater(HttpTheater newTheater)
            throws MovieException {
        Theater theaterToCreate = convert(newTheater);
        Theater addedTheater = theaterService.addTheater(theaterToCreate);
        return Response.status(Status.CREATED)
                .header("Location", "/theaters/" + addedTheater.getTheaterId())
                .entity(new HttpTheater(addedTheater)).build();
    }

    @DELETE
    @Path("/delete/{theaterId}")
    public Response deleteTheater(@PathParam("theaterId") long theaterId,
            HttpTheater newTheater) throws MovieException {
        Theater deleteTheater = theaterService.deleteTheater(theaterId);
        return Response.status(Status.OK)
                .header("Location", "/theater/" + deleteTheater.getTheaterId())
                .entity(new HttpTheater(deleteTheater)).build();
    }

    @PUT
    @Path("/update")
    public Response updateTheater(@QueryParam("theaterId") long theaterId,
            HttpTheater updateTheater) throws MovieException {
        Theater theaterToUpdate = convert(updateTheater);
        theaterService.updateTheater(theaterToUpdate, theaterId);
        Theater updatedTheater = theaterService.getTheater(theaterId);
        return Response.status(Status.OK)
                .header("Location", "/theater/" + updatedTheater.getTheaterId())
                .entity(new HttpTheater(updatedTheater)).build();
    }

    @GET
    @Path("/{theaterId}")
    public HttpTheater getTheaterById(@PathParam("theaterId") long theaterId)
            throws MovieException {
        logger.info("getting theater by id:" + theaterId);
        Theater theater = theaterService.getTheater(theaterId);
        if (theater != null)
            return new HttpTheater(theater);
        else
            return null;
    }

    @GET
    @Path("/all")
    public List<HttpTheater> getTheater() throws MovieException {
        logger.info("getting all Theater");
        List<Theater> theater = theaterService.getAllTheaters();

        List<HttpTheater> httpTheater = new ArrayList<HttpTheater>();

        for (Theater t : theater) {
            httpTheater.add(new HttpTheater(t));
        }
        return httpTheater;
    }

    @GET
    @Path("/")
    @Wrapped(element = "theater")
    public List<HttpTheater> getTheaterSearch(
            @QueryParam("theaterName") String theaterName)
                    throws MovieException {
        logger.info("theaterName search =" + theaterName);
        List<Theater> found = theaterService.searchTheater(theaterName);
        List<HttpTheater> returnList = new ArrayList<>(found.size());
        for (Theater theater : found) {
            returnList.add(new HttpTheater(theater));
        }
        return returnList;
    }

    private Theater convert(HttpTheater httpTheater) {
        if (httpTheater == null) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "Theater object is not provided in body");
        }

        TheaterImpl newTheater = new TheaterImpl();

        newTheater.setTheaterName(httpTheater.theaterName);
        newTheater.setTheaterCity(httpTheater.theaterCity);
        newTheater.setTheaterState(httpTheater.theaterState);
        newTheater.setTheaterZip(httpTheater.theaterZip);
        newTheater.setPhone(httpTheater.theaterPhone);
        newTheater.setTheaterAminities(httpTheater.theaterAminities);

        return newTheater;
    }

}
