package com.rashmi.movies.http.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;

import org.jboss.resteasy.annotations.providers.jaxb.json.Mapped;

import com.rashmi.movies.entity.GetShowTime;

@Mapped
@XmlRootElement(name = "showtime")
@XmlAccessorType(XmlAccessType.NONE)
public class HttpGetShowTime {
    @XmlElement
    private long id;

    @XmlElement
    private String movieName;

    @XmlElement
    private String theaterName;

    @XmlElement
    private String theaterCity;

    @XmlElement
    private String theaterState;

    @XmlElement
    private long theaterZip;

    @XmlElement
    private String theaterPhone;

    @XmlElement
    private String theaterAminities;

    @XmlElement
    private String ShowTime;

    @XmlElement
    private long price;

    @XmlElement
    private String movieDetails;

    @XmlElement(name = "status")
    public String status;

    @XmlElement(name = "fromDate")
    @XmlSchemaType(name = "date")
    public Date fromDate;

    @XmlElement(name = "toDate")
    @XmlSchemaType(name = "date")
    public Date toDate;

    protected HttpGetShowTime() {
    }

    public HttpGetShowTime(GetShowTime showtime) {
        this.id = showtime.getId();
        this.movieName = showtime.getMovieName();
        this.movieDetails = showtime.getMovieDetails();
        this.theaterName = showtime.getTheaterName();
        this.theaterCity = showtime.getTheaterCity();
        this.theaterState = showtime.getTheaterState();
        this.theaterZip = showtime.getTheaterZip();
        this.theaterPhone = showtime.getTheaterPhone();
        this.theaterAminities = showtime.getTheaterAminities();
        this.ShowTime = showtime.getShowTime();
        this.price = showtime.getPrice();
        this.status = showtime.getStatus();
        this.fromDate = showtime.getFromDate();
        this.toDate = showtime.getToDate();
    }
}
