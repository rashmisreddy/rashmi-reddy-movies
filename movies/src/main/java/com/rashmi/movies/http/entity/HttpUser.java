package com.rashmi.movies.http.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.jboss.resteasy.annotations.providers.jaxb.json.Mapped;

import com.rashmi.movies.entity.User;

/**
 * Select fields we want exposed to the REST layer. Separation from
 * business/data layer.
 * 
 * Note "XML" annotation. The resteasy-jettison implementation converts these to
 * JSON depending on the Accept media type
 * 
 */
@Mapped
@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class HttpUser {

    @XmlElement
    public long id;

    @XmlElement
    public String firstName;

    @XmlElement
    public String lastName;

    @XmlElement
    public String password;

    @XmlElement
    public String paymentType;

    @XmlElement
    public String emailId;

    @XmlElement
    public long phone;

    @XmlElement(name = "address", required = true, type = HttpAddress.class)
    public HttpAddress newAddress;

    protected HttpUser() {
    }

    public HttpUser(User user) {
        this.id = user.getUserId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.password = user.getPassword();
        this.paymentType = user.getPaymentType();
        this.emailId = user.getEmailId();
        this.phone = user.getPhone();
        this.newAddress = convertAddress(user);
    }

    private HttpAddress convertAddress(User user) {
        HttpAddress newHttpAddress = new HttpAddress();

        newHttpAddress.street1 = user.getAddress().getStreet1();
        newHttpAddress.street2 = user.getAddress().getStreet2();
        newHttpAddress.city = user.getAddress().getCity();
        newHttpAddress.state = user.getAddress().getState();
        newHttpAddress.zip = user.getAddress().getZip();
        newHttpAddress.country = user.getAddress().getCountry();

        return newHttpAddress;
    }
}