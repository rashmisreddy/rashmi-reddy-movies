package com.rashmi.movies.http.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.jboss.resteasy.annotations.providers.jaxb.json.Mapped;

import com.rashmi.movies.entity.GetBuyTicket;

@Mapped
@XmlRootElement(name = "buyTicket")
@XmlAccessorType(XmlAccessType.NONE)
public class HttpGetBuyTicket {
    @XmlElement
    private long txnid;

    @XmlElement
    private String movieName;

    @XmlElement
    private String theaterName;

    @XmlElement
    private String emailId;

    @XmlElement
    private long nooftickets;

    @XmlElement
    private long totalprice;

    @XmlElement
    private String status;

    @XmlElement
    private Date date;

    protected HttpGetBuyTicket() {
    }

    public HttpGetBuyTicket(GetBuyTicket getBuyTicket) {
        this.txnid = getBuyTicket.getTxnid();
        this.movieName = getBuyTicket.getMovieName();
        this.theaterName = getBuyTicket.getTheaterName();
        this.emailId = getBuyTicket.getEmailId();
        this.nooftickets = getBuyTicket.getNooftickets();
        this.status = getBuyTicket.getStatus();
        this.totalprice = getBuyTicket.getTotalPrice();
        this.date = getBuyTicket.getDate();
    }
}
