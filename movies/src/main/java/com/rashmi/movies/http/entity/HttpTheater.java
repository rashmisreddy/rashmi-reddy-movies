package com.rashmi.movies.http.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.jboss.resteasy.annotations.providers.jaxb.json.Mapped;

import com.rashmi.movies.entity.Theater;

/**
 * Select fields we want exposed to the REST layer. Separation from
 * business/data layer.
 * 
 * Note "XML" annotation. The resteasy-jettison implementation converts these to
 * JSON depending on the Accept media type
 * 
 */
@Mapped
@XmlRootElement(name = "theater")
@XmlAccessorType(XmlAccessType.NONE)
public class HttpTheater {

    @XmlElement
    public long id;

    @XmlElement
    public String theaterName;

    @XmlElement
    public String theaterCity;

    @XmlElement
    public String theaterState;

    @XmlElement
    public long theaterZip;

    @XmlElement
    public String theaterPhone;

    @XmlElement
    public String theaterAminities;

    // required by framework
    protected HttpTheater() {
    }

    public HttpTheater(Theater theater) {
        this.id = theater.getTheaterId();
        this.theaterName = theater.getTheaterName();
        this.theaterCity = theater.getTheaterCity();
        this.theaterState = theater.getTheaterState();
        this.theaterZip = theater.getTheaterZip();
        this.theaterPhone = theater.getPhone();
        this.theaterAminities = theater.getTheaterAminities();
    }
}
