package com.rashmi.movies.http.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;

import org.jboss.resteasy.annotations.providers.jaxb.json.Mapped;

import com.rashmi.movies.entity.PostBuyTicket;

@Mapped
@XmlRootElement(name = "buyTicket")
@XmlAccessorType(XmlAccessType.NONE)
public class HttpPostBuyTicket {
    @XmlElement
    private String movieName;

    @XmlElement
    private String theaterName;

    @XmlElement
    private String emailId;

    @XmlElement
    private long nooftickets;
    
    @XmlElement
    private String showTime;
    
    @XmlElement
    @XmlSchemaType(name = "date")
    private Date date;

    protected HttpPostBuyTicket() {
    }

    public HttpPostBuyTicket(PostBuyTicket postBuyTicket) {
        this.movieName = postBuyTicket.getMovieName();
        this.theaterName = postBuyTicket.getTheaterName();
        this.emailId = postBuyTicket.getEmailId();
        this.nooftickets = postBuyTicket.getNoOfTickets();
        this.showTime = postBuyTicket.getShowTime();
        this.date = postBuyTicket.getDate();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMovieName() {
        return movieName;
    }

    public String getTheaterName() {
        return theaterName;
    }

    public long getNooftickets() {
        return nooftickets;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public void setTheaterName(String theaterName) {
        this.theaterName = theaterName;
    }

    public void setNooftickets(long nooftickets) {
        this.nooftickets = nooftickets;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getShowTime() {
        return showTime;
    }

    public void setShowTime(String showTime) {
        this.showTime = showTime;
    }
}
