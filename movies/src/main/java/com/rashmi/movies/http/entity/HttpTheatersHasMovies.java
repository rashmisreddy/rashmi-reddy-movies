package com.rashmi.movies.http.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;

import org.jboss.resteasy.annotations.providers.jaxb.json.Mapped;

import com.rashmi.movies.entity.TheatersHasMovies;
import com.rashmi.movies.service.exception.MovieException;

@Mapped
@XmlRootElement(name = "theater-movie")
@XmlAccessorType(XmlAccessType.NONE)
public class HttpTheatersHasMovies {
    @XmlElement(name = "id")
    public long id;

    @XmlElement(name = "theaterid")
    public long theaterid;

    @XmlElement(name = "movieid")
    public long movieid;

    @XmlElement(name = "showtime")
    public String showtime;

    @XmlElement(name = "fromDate")
    @XmlSchemaType(name = "date")
    public Date fromDate;

    @XmlElement(name = "toDate")
    @XmlSchemaType(name = "date")
    public Date toDate;

    @XmlElement(name = "price")
    public long price;

    @XmlElement(name = "status")
    public String status;

    protected HttpTheatersHasMovies() {
    }

    public HttpTheatersHasMovies(TheatersHasMovies theatersHasMovies)
            throws MovieException {
        this.id = theatersHasMovies.getId();
        this.theaterid = theatersHasMovies.getTheaterid();
        this.movieid = theatersHasMovies.getMovieid();
        this.showtime = theatersHasMovies.getShowtime();
        this.price = theatersHasMovies.getPrice();
        this.status = theatersHasMovies.getStatus();
        this.fromDate = theatersHasMovies.getFromDate();
        this.toDate = theatersHasMovies.getToDate();
    }

    public long getId() {
        return id;
    }

    public long getTheaterid() {
        return theaterid;
    }

    public long getMovieid() {
        return movieid;
    }

    public String getShowtime() {
        return showtime;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public long getPrice() {
        return price;
    }

    public String getStatus() {
        return status;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTheaterid(long theaterid) {
        this.theaterid = theaterid;
    }

    public void setMovieid(long movieid) {
        this.movieid = movieid;
    }

    public void setShowtime(String showtime) {
        this.showtime = showtime;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
