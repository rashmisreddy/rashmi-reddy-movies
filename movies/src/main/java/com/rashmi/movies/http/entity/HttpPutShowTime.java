//package com.rashmi.movies.http.entity;
//
//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlSchemaType;
//
//import org.jboss.resteasy.annotations.providers.jaxb.json.Mapped;
//
//import com.rashmi.movies.entity.PutShowTime;
//
//@Mapped
//@XmlRootElement(name = "showtime")
//@XmlAccessorType(XmlAccessType.NONE)
//public class HttpPutShowTime {
//    @XmlElement(name = "ShowTime")
//    private String ShowTime;
//
//    @XmlElement(name = "price")
//    private long price;
//
//    @XmlElement(name = "status")
//    public String status;
//
//    @XmlElement(name = "fromDate")
//    @XmlSchemaType(name = "date")
//    public String fromDate;
//
//    @XmlElement(name = "toDate")
//    @XmlSchemaType(name = "date")
//    public String toDate;
//
//    protected HttpPutShowTime() {
//    }
//
//    public HttpPutShowTime(PutShowTime putShowtime) {
//        this.ShowTime = putShowtime.getShowTime();
//        this.price = putShowtime.getPrice();
//        this.status = putShowtime.getStatus();
//        this.fromDate = putShowtime.getFromDate();
//        this.toDate = putShowtime.getToDate();
//    }
//
//    public String getShowTime() {
//        return ShowTime;
//    }
//
//    public long getPrice() {
//        return price;
//    }
//
//    public void setShowTime(String showTime) {
//        ShowTime = showTime;
//    }
//
//    public void setPrice(long price) {
//        this.price = price;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public String getFromDate() {
//        return fromDate;
//    }
//
//    public String getToDate() {
//        return toDate;
//    }
//
//    public void setFromDate(String fromDate) {
//        this.fromDate = fromDate;
//    }
//
//    public void setToDate(String toDate) {
//        this.toDate = toDate;
//    }
//}