package com.rashmi.movies.http;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rashmi.movies.entity.Address;
import com.rashmi.movies.entity.User;
import com.rashmi.movies.entity.impl.AddressImpl;
import com.rashmi.movies.entity.impl.UserImpl;
import com.rashmi.movies.http.entity.HttpAddress;
import com.rashmi.movies.http.entity.HttpUser;
import com.rashmi.movies.service.UserService;
import com.rashmi.movies.service.exception.MovieException;

@Path("/users")
@ApplicationPath("/users")
@Component
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public class UserResource extends Application {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserService userService;

    @POST
    @Path("/")
    public Response createUser(HttpUser newUser) throws MovieException {
        User userToCreate = convertUser(newUser);
        User addedUser = userService.addUser(userToCreate);
        return Response.status(Status.CREATED)
                .header("Location", "/users/" + addedUser.getUserId())
                .entity(new HttpUser(addedUser)).build();
    }

    @DELETE
    @Path("/delete/{userId}")
    public Response deleteUser(@PathParam("userId") long userId,
            HttpUser newUser) throws MovieException {
        User deleteUser = userService.deleteUser(userId);
        return Response.status(Status.OK)
                .header("Location", "/users/" + deleteUser.getUserId())
                .entity(new HttpUser(deleteUser)).build();
    }

    @PUT
    @Path("/update")
    public Response updateUser(@QueryParam("userId") long userId,
            HttpUser updateUser) throws MovieException {
        User userToUpdate = convertUser(updateUser);
        userService.updateUser(userToUpdate, userId);
        User updatedUser = userService.getUser(userId);
        return Response.status(Status.OK)
                .header("Location", "/users/" + updatedUser.getUserId())
                .entity(new HttpUser(updatedUser)).build();
    }

    @GET
    @Path("/{userId}")
    public HttpUser getUserById(@PathParam("userId") long userId)
            throws MovieException {
        logger.info("getting user by id:" + userId);
        User user = userService.getUser(userId);
        if (user != null)
            return new HttpUser(user);
        else
            return null;
    }

    @GET
    @Path("/")
    @Wrapped(element = "users")
    public List<HttpUser> getUserSearch(
            @QueryParam("firstName") String firstName,
            @QueryParam("lastName") String lastName) throws MovieException {
        logger.info(
                "user search firstName=" + firstName + " lastName=" + lastName);
        List<User> found = userService.getUsers(firstName, lastName);
        List<HttpUser> returnList = new ArrayList<>(found.size());
        for (User user : found) {
            returnList.add(new HttpUser(user));
        }
        return returnList;
    }

    private User convertUser(HttpUser httpUser) {
        UserImpl newUser = new UserImpl();

        if(httpUser == null){
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "User object is not provided in body");
        }
        Address newAddress = createAddress(httpUser.newAddress);

        newUser.setUserId(httpUser.id);
        newUser.setFirstName(httpUser.firstName);
        newUser.setLastName(httpUser.lastName);
        newUser.setEmailId(httpUser.emailId);
        newUser.setPassword(httpUser.password);
        newUser.setPaymentType(httpUser.paymentType);
        newUser.setPhone(httpUser.phone);
        // Adding address to both sides
        newUser.setAddress(newAddress);
        newAddress.setUser(newUser);

        return newUser;
    }

    private Address createAddress(HttpAddress newHttpAddress) {
        if(newHttpAddress == null){
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "Address object is not provided in body");
        }
        AddressImpl makeNewAddress = new AddressImpl();

        makeNewAddress.setStreet1(newHttpAddress.getStreet1());
        makeNewAddress.setStreet2(newHttpAddress.getStreet2());
        makeNewAddress.setCity(newHttpAddress.getCity());
        makeNewAddress.setState(newHttpAddress.getState());
        makeNewAddress.setZip(newHttpAddress.getZip());
        makeNewAddress.setCountry(newHttpAddress.getCountry());

        return makeNewAddress;
    }
}
