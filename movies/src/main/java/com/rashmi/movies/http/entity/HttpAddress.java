package com.rashmi.movies.http.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.jboss.resteasy.annotations.providers.jaxb.json.Mapped;

import com.rashmi.movies.entity.Address;

@Mapped
@XmlRootElement(name = "address")
@XmlAccessorType(XmlAccessType.FIELD)
public class HttpAddress {
    @XmlElement
    public long id;

    @XmlElement
    public String street1;

    @XmlElement
    public String street2;

    @XmlElement
    public String city;

    @XmlElement
    public String state;

    @XmlElement
    public long zip;

    @XmlElement
    public String country;

    // required by framework
    protected HttpAddress() {
    }

    public HttpAddress(Address address) {
        this.id = address.getId();
        this.street1 = address.getStreet1();
        this.street2 = address.getStreet2();
        this.city = address.getCity();
        this.state = address.getState();
        this.zip = address.getZip();
        this.country = address.getCountry();
    }

    public long getId() {
        return id;
    }

    public String getStreet1() {
        return street1;
    }

    public String getStreet2() {
        return street2;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public long getZip() {
        return zip;
    }

    public String getCountry() {
        return country;
    }
}
