package com.rashmi.movies.http.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.jboss.resteasy.annotations.providers.jaxb.json.Mapped;

import com.rashmi.movies.entity.Movie;
import com.rashmi.movies.entity.User;

/**
 * Select fields we want exposed to the REST layer. Separation from
 * business/data layer.
 * 
 * Note "XML" annotation. The resteasy-jettison implementation converts these to
 * JSON depending on the Accept media type
 * 
 */
@Mapped
@XmlRootElement(name = "movie")
@XmlAccessorType(XmlAccessType.NONE)
public class HttpMovie {

    @XmlElement
    public long id;

    @XmlElement
    public String movieName;

    @XmlElement
    public String movieDetails;

    // required by framework
    protected HttpMovie() {
    }

    public HttpMovie(Movie movie) {
        this.id = movie.getMovieId();
        this.movieName = movie.getMovieName();
        this.movieDetails = movie.getMovieDetails();
    }
}
