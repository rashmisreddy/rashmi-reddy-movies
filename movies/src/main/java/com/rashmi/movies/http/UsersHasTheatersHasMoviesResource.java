package com.rashmi.movies.http;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rashmi.movies.entity.GetBuyTicket;
import com.rashmi.movies.entity.PostBuyTicket;
import com.rashmi.movies.entity.UsersHasTheatersHasMovies;
import com.rashmi.movies.entity.impl.PostBuyTicketImpl;
import com.rashmi.movies.http.entity.HttpGetBuyTicket;
import com.rashmi.movies.http.entity.HttpPostBuyTicket;
import com.rashmi.movies.http.entity.HttpUsersHasTheatersHasMovies;
import com.rashmi.movies.service.UsersHasTheatersHasMoviesService;
import com.rashmi.movies.service.exception.MovieException;

/*
 * We cannot delete a Transaction, hence not implementing @DELETE
 */
@Path("/Ticket")
@ApplicationPath("/Ticket")
@Component
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public class UsersHasTheatersHasMoviesResource extends Application {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UsersHasTheatersHasMoviesService userHasTheatersHasMoviesService;

    @GET
    @Path("/")
    @Wrapped(element = "getBuyTicket")
    public HttpGetBuyTicket getTxn(@QueryParam("txnId") long txnId)
            throws MovieException {
        if (txnId != 0L) {
            logger.info("getting ticket by txnid:" + txnId);
            GetBuyTicket getBuyTicket = userHasTheatersHasMoviesService
                    .getTicket(txnId);
            if (getBuyTicket != null)
                return new HttpGetBuyTicket(getBuyTicket);
            else
                return null;
        } else {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
                    "txnId is not provided");
        }
    }

    @POST
    @Path("/")
    public Response createTxn(HttpPostBuyTicket newPostBuyTicket)
            throws MovieException {
        System.out.println(">>>>>>Date is : " + newPostBuyTicket.getDate());
        PostBuyTicket txnToCreate = convertPostBuyTicket(newPostBuyTicket);
        // txnToCreate.setStatus("INITATED");

        GetBuyTicket addedTxn = userHasTheatersHasMoviesService
                .addTxn(txnToCreate);
        return Response.status(Status.CREATED)
                .header("Location", "/buy/" + addedTxn.getTxnid())
                .entity(new HttpGetBuyTicket(addedTxn)).build();
    }

    @PUT
    @Path("/update")
    public Response updateUser(@QueryParam("txnId") long txnId,
            @QueryParam("status") String status) throws MovieException {
        userHasTheatersHasMoviesService.updateTxn(txnId, status);
        UsersHasTheatersHasMovies updatedUsersHasTheatersHasMovies = userHasTheatersHasMoviesService
                .getTxn(txnId);
        return Response.status(Status.OK)
                .header("Location",
                        "/Ticket/"
                                + updatedUsersHasTheatersHasMovies.getTxnid())
                .entity(new HttpUsersHasTheatersHasMovies(
                        updatedUsersHasTheatersHasMovies))
                .build();
    }

    private PostBuyTicket convertPostBuyTicket(
            HttpPostBuyTicket httpPostBuyTicket) {

        PostBuyTicketImpl newPostBuyTicket = new PostBuyTicketImpl();

        // TODO : Add firstname and lastname along with emailId
        newPostBuyTicket.setEmailId(httpPostBuyTicket.getEmailId());
        // newPostBuyTicket.setFirstName(httpPostBuyTicket.getFirstName());
        // newPostBuyTicket.setLastName(httpPostBuyTicket.getLastName());
        newPostBuyTicket.setMovieName(httpPostBuyTicket.getMovieName());
        newPostBuyTicket.setTheaterName(httpPostBuyTicket.getTheaterName());
        newPostBuyTicket.setNoOfTickets(httpPostBuyTicket.getNooftickets());
        newPostBuyTicket.setShowTime(httpPostBuyTicket.getShowTime());
        newPostBuyTicket.setDate(httpPostBuyTicket.getDate());

        return newPostBuyTicket;
    }
}
