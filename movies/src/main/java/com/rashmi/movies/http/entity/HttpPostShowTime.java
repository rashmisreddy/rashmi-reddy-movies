package com.rashmi.movies.http.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;

import org.jboss.resteasy.annotations.providers.jaxb.json.Mapped;

import com.rashmi.movies.entity.PostShowTime;

@Mapped
@XmlRootElement(name = "showtime")
@XmlAccessorType(XmlAccessType.FIELD)
public class HttpPostShowTime {
    @XmlElement(name = "movieName")
    private String movieName;

    @XmlElement(name = "theaterName")
    private String theaterName;

    @XmlElement(name = "ShowTime")
    private String ShowTime;

    @XmlElement(name = "price")
    private long price;

    @XmlElement(name = "status")
    public String status;

    @XmlElement(name = "fromDate")
    @XmlSchemaType(name = "date")
    public Date fromDate;

    @XmlElement(name = "toDate")
    @XmlSchemaType(name = "date")
    public Date toDate;

    protected HttpPostShowTime() {
    }

    public HttpPostShowTime(PostShowTime postShowtime) {
        this.movieName = postShowtime.getMovieName();
        this.theaterName = postShowtime.getTheaterName();
        this.ShowTime = postShowtime.getShowTime();
        this.price = postShowtime.getPrice();
        this.status = postShowtime.getStatus();
        this.fromDate = postShowtime.getFromDate();
        this.toDate = postShowtime.getToDate();
    }

    public String getMovieName() {
        return movieName;
    }

    public String getTheaterName() {
        return theaterName;
    }

    public String getShowTime() {
        return ShowTime;
    }

    public long getPrice() {
        return price;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public void setTheaterName(String theaterName) {
        this.theaterName = theaterName;
    }

    public void setShowTime(String showTime) {
        ShowTime = showTime;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}
