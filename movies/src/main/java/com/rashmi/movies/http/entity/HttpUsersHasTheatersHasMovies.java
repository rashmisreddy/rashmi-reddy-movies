package com.rashmi.movies.http.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.jboss.resteasy.annotations.providers.jaxb.json.Mapped;

import com.rashmi.movies.entity.UsersHasTheatersHasMovies;

@Mapped
@XmlRootElement(name = "Ticket")
@XmlAccessorType(XmlAccessType.NONE)
public class HttpUsersHasTheatersHasMovies {
    @XmlElement(name = "txnid")
    public long txnid;

    @XmlElement(name = "Theaters_has_Movies_id")
    public long theatershasmoviesid;

    @XmlElement(name = "usersuserid")
    public long userid;

    @XmlElement(name = "nooftickets")
    public long nooftickets;

    @XmlElement(name = "totalprice")
    public long totalprice;

    @XmlElement(name = "status")
    public String status;

    @XmlElement(name = "date")
    public Date date;

    protected HttpUsersHasTheatersHasMovies() {
    }

    public HttpUsersHasTheatersHasMovies(
            UsersHasTheatersHasMovies usersHasTheatersHasMovies) {
        this.txnid = usersHasTheatersHasMovies.getTxnid();
        this.theatershasmoviesid = usersHasTheatersHasMovies
                .getTheatershasmoviesid();
        this.userid = usersHasTheatersHasMovies.getUserid();
        this.totalprice = usersHasTheatersHasMovies.getTotalprice();
        this.nooftickets = usersHasTheatersHasMovies.getNooftickets();
        this.status = usersHasTheatersHasMovies.getStatus();
        this.date = usersHasTheatersHasMovies.getDate();
    }
}
