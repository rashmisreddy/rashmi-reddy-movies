package com.rashmi.movies.http;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rashmi.movies.entity.GetShowTime;
import com.rashmi.movies.entity.PostShowTime;
import com.rashmi.movies.entity.impl.PostShowTimeImpl;
import com.rashmi.movies.http.entity.HttpGetShowTime;
import com.rashmi.movies.http.entity.HttpPostShowTime;
import com.rashmi.movies.service.TheatersHasMoviesService;
import com.rashmi.movies.service.exception.MovieException;

@Path("/showtimes")
@ApplicationPath("/showtimes")
@Component
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public class TheatersHasMoviesResource extends Application {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TheatersHasMoviesService theatersHasMoviesService;

    @POST
    @Path("/")
    public Response createTheatersHasMovies(
            HttpPostShowTime newHttpPostShowTime)
                    throws MovieException, ParseException {
        PostShowTime postShowTimeToCreate = convertPostShowTime(
                newHttpPostShowTime);

        GetShowTime addedTheatersHasMovies = theatersHasMoviesService
                .addShowTimes(postShowTimeToCreate);
        return Response.status(Status.CREATED)
                .header("Location",
                        "/theatersHasMovies/" + addedTheatersHasMovies.getId())
                .entity(new HttpGetShowTime(addedTheatersHasMovies)).build();

    }

    private PostShowTime convertPostShowTime(
            HttpPostShowTime httpPostShowTime) {
        PostShowTimeImpl newPostShowTime = new PostShowTimeImpl();

        newPostShowTime.setMovieName(httpPostShowTime.getMovieName());
        newPostShowTime.setTheaterName(httpPostShowTime.getTheaterName());
        newPostShowTime.setShowTime(httpPostShowTime.getShowTime());
        newPostShowTime.setPrice(httpPostShowTime.getPrice());
        newPostShowTime.setStatus(httpPostShowTime.getStatus());
        newPostShowTime.setFromDate(httpPostShowTime.getFromDate());
        newPostShowTime.setToDate(httpPostShowTime.getToDate());

        return newPostShowTime;
    }

    @PUT
    @Path("/update/")
    public Response updateShowTime(HttpPostShowTime newHttpPutShowTime)
            throws MovieException {
        PostShowTime putShowTimeToUpdate = convertPostShowTime(
                newHttpPutShowTime);

        GetShowTime upatedTheatersHasMovies = theatersHasMoviesService
                .updateShowTimes(putShowTimeToUpdate);

        return Response.status(Status.OK)
                .header("Location",
                        "/theatersHasMovies/" + upatedTheatersHasMovies.getId())
                .entity(new HttpGetShowTime(upatedTheatersHasMovies)).build();
    }

    @GET
    @Path("/byMovieName")
    @Wrapped(element = "showtimes")
    public List<HttpGetShowTime> getShowTimesByMovieName(
            @QueryParam("movieName") String movieName) throws MovieException {
        logger.info("getting showtimes by and movieName:" + movieName);

        List<GetShowTime> found = theatersHasMoviesService
                .getShowTimesByMovieName(movieName);

        List<HttpGetShowTime> returnList = new ArrayList<>(found.size());
        for (GetShowTime thm : found) {
            returnList.add(new HttpGetShowTime(thm));
        }
        return returnList;
    }

    @GET
    @Path("/byTheaterName")
    @Wrapped(element = "getShowTimes")
    public List<HttpGetShowTime> getShowTimesByTheaterName(
            @QueryParam("theaterName") String theaterName)
                    throws MovieException {
        logger.info("getting showtimes by and theaterName:" + theaterName);

        List<GetShowTime> found = theatersHasMoviesService
                .getShowTimesByTheaterName(theaterName);

        List<HttpGetShowTime> returnList = new ArrayList<>(found.size());
        for (GetShowTime thm : found) {
            returnList.add(new HttpGetShowTime(thm));
        }
        return returnList;
    }
}