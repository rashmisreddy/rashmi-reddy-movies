package com.rashmi.movies.entity;

import java.util.Date;

public interface GetShowTime {

    String getMovieName();

    String getTheaterName();

    String getTheaterCity();

    String getTheaterState();

    String getTheaterPhone();

    String getTheaterAminities();

    String getShowTime();

    long getPrice();

    void setMovieName(String movieName);

    void setTheaterName(String theaterName);

    void setTheaterCity(String theaterCity);

    void setTheaterState(String theaterState);

    void setTheaterPhone(String theaterPhone);

    void setTheaterAminities(String theaterAminities);

    void setShowTime(String showTime);

    String getMovieDetails();

    void setMovieDetails(String movieDetails);

    long getTheaterZip();

    void setTheaterZip(long theaterZip);

    void setPrice(long price);

    String getStatus();

    void setStatus(String status);

    int getId();

    void setId(int id);

    Date getFromDate();

    Date getToDate();

    void setFromDate(Date fromDate);

    void setToDate(Date toDate);
}
