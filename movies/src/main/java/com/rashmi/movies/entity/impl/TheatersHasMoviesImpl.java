package com.rashmi.movies.entity.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.rashmi.movies.entity.TheatersHasMovies;
import com.rashmi.movies.entity.User;

@Entity
@Table(name = "Theaters_has_Movies")
public class TheatersHasMoviesImpl implements TheatersHasMovies {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "Movies_movie_id")
    private long movieid;

    @Column(name = "Theaters_theater_id")
    private long theaterid;

    @Column(name = "showtime")
    // @Temporal(TemporalType.DATE)
    // @DateTimeFormat(iso = DateTimeFormat.ISO.NONE)
    private String showtime;

    @Column(name = "from_date")
    private Date fromDate;

    @Column(name = "to_date")
    private Date toDate;

    @Column(name = "price")
    private long price;

    @Column(name = "status")
    private String status;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "listOfTheatersHasMovies", targetEntity = UserImpl.class)
    List<User> listOfUsers = new ArrayList<User>();

    public TheatersHasMoviesImpl() {
    }

    public String getShowtime() {
        return showtime;
    }

    public void setShowtime(String showtime) {
        this.showtime = showtime;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMovieid() {
        return movieid;
    }

    public long getTheaterid() {
        return theaterid;
    }

    public void setMovieid(long movieid) {
        this.movieid = movieid;
    }

    public void setTheaterid(long theaterid) {
        this.theaterid = theaterid;
    }

    public List<User> getListOfUsers() {
        return listOfUsers;
    }

    public void setListOfUsers(List<User> listOfUsers) {
        this.listOfUsers = listOfUsers;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}
