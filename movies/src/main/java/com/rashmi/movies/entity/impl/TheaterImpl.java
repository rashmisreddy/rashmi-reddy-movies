package com.rashmi.movies.entity.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.rashmi.movies.entity.Movie;
import com.rashmi.movies.entity.Theater;

@Entity
@Table(name = "Theaters")
public class TheaterImpl implements Theater {
    @Id
    @Column(name = "theater_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long theaterId;
    @Column(name = "theater_name")
    private String theaterName;
    @Column(name = "theater_city")
    private String theaterCity;
    @Column(name = "theater_state")
    private String theaterState;
    @Column(name = "theater_zip")
    private long theaterZip;
    @Column(name = "phone")
    private String phone;
    @Column(name = "aminities")
    private String theaterAminities;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "listOfTheaters", targetEntity = MovieImpl.class, cascade = CascadeType.ALL)
    List<Movie> listOfMovies = new ArrayList<Movie>();

    public TheaterImpl() {
    }

    public TheaterImpl(long theaterId) {
        this.theaterId = theaterId;
    }

    public long getTheaterId() {
        return theaterId;
    }

    public String getTheaterName() {
        return theaterName;
    }

    public String getPhone() {
        return phone;
    }

    public String getTheaterAminities() {
        return theaterAminities;
    }

    public void setTheaterId(long theaterId) {
        this.theaterId = theaterId;
    }

    public void setTheaterName(String theaterName) {
        this.theaterName = theaterName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setTheaterAminities(String theaterAminities) {
        this.theaterAminities = theaterAminities;
    }

    public String getTheaterCity() {
        return theaterCity;
    }

    public String getTheaterState() {
        return theaterState;
    }

    public long getTheaterZip() {
        return theaterZip;
    }

    public void setTheaterCity(String theaterCity) {
        this.theaterCity = theaterCity;
    }

    public void setTheaterState(String theaterState) {
        this.theaterState = theaterState;
    }

    public void setTheaterZip(long theaterZip) {
        this.theaterZip = theaterZip;
    }

    public String getTheaterName(long addedTheaterId) {
        return this.theaterName;
    }

    public List<Movie> getMovies() {
        return listOfMovies;
    }

    public void addMovies(Movie movie) {
        if (this.listOfMovies == null) {
            this.listOfMovies = new ArrayList<Movie>();
        }
        this.listOfMovies.add(movie);
    }

    public void setMovies(List<Movie> movies) {
        this.listOfMovies = movies;
    }
}