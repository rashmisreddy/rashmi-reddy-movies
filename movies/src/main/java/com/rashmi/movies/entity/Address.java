package com.rashmi.movies.entity;

public interface Address {
    String getStreet1();

    String getStreet2();

    String getCity();

    String getState();

    long getZip();

    String getCountry();

    User getUser();

    void setUser(User newUser);
    
    long getId();
}