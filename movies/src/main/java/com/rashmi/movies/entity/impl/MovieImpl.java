package com.rashmi.movies.entity.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.rashmi.movies.entity.Movie;
import com.rashmi.movies.entity.Theater;

@Entity
@Table(name = "Movies")
public class MovieImpl implements Movie {
    @Id
    @Column(name = "movie_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long movieId;
    @Column(name = "movie_name")
    private String movieName;
    @Column(name = "movie_details")
    private String movieDetails;

    @ManyToMany(targetEntity = TheaterImpl.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "Theaters_has_movies", joinColumns = {
            @JoinColumn(name = "Movies_movie_id", nullable = false) }, inverseJoinColumns = {
                    @JoinColumn(name = "Theaters_theater_id", nullable = false) })
    List<Theater> listOfTheaters = new ArrayList<Theater>();

    public MovieImpl() {
    }

    public MovieImpl(long movieId) {
        this.movieId = movieId;
    }

    public String getMovieName() {
        return movieName;
    }

    public String getMovieDetails() {
        return movieDetails;
    }

    public long getMovieId() {
        return movieId;
    }

    public void setMovieId(long movieId) {
        this.movieId = movieId;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public void setMovieDetails(String movieDetails) {
        this.movieDetails = movieDetails;
    }

    @Override
    public String getMovieName(long movieid) {
        return this.movieName;
    }

    @Override
    public String getMovieDetails(long movieid) {
        return this.movieDetails;
    }

    public List<Theater> getTheaters() {
        return listOfTheaters;
    }

    public void setTheaters(List<Theater> theaters) {
        this.listOfTheaters = theaters;
    }

    @Override
    public void addTheater(Theater theater) {
        if (listOfTheaters == null) {
            listOfTheaters = new ArrayList<Theater>();
        }
        listOfTheaters.add(theater);
    }
}