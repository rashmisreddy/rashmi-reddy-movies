package com.rashmi.movies.entity;

import java.util.Date;

public interface PostBuyTicket {
    String getMovieName();

    String getTheaterName();

    void setMovieName(String movieName);

    void setTheaterName(String theaterName);

    long getNoOfTickets();

    void setNoOfTickets(long noOfTickets);

    String getEmailId();

    void setEmailId(String emailId);

    String getShowTime();

    void setShowTime(String showTime);

     Date getDate();

     void setDate(Date date);
}
