package com.rashmi.movies.entity.impl;

import java.util.Date;

import com.rashmi.movies.entity.GetShowTime;

public class GetShowTimeImpl implements GetShowTime {
    private int id;
    private String movieName;
    private String theaterName;
    private String theaterCity;
    private String theaterState;
    private long theaterZip;
    private String theaterPhone;
    private String theaterAminities;
    private String ShowTime;
    private long price;
    private String movieDetails;
    private String status;
    private Date fromDate;
    private Date toDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public GetShowTimeImpl() {
    }

    public String getMovieName() {
        return movieName;
    }

    public String getTheaterName() {
        return theaterName;
    }

    public String getTheaterCity() {
        return theaterCity;
    }

    public String getTheaterState() {
        return theaterState;
    }

    public String getTheaterPhone() {
        return theaterPhone;
    }

    public String getTheaterAminities() {
        return theaterAminities;
    }

    public String getShowTime() {
        return ShowTime;
    }

    public long getPrice() {
        return price;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public void setTheaterName(String theaterName) {
        this.theaterName = theaterName;
    }

    public void setTheaterCity(String theaterCity) {
        this.theaterCity = theaterCity;
    }

    public void setTheaterState(String theaterState) {
        this.theaterState = theaterState;
    }

    public void setTheaterPhone(String theaterPhone) {
        this.theaterPhone = theaterPhone;
    }

    public void setTheaterAminities(String theaterAminities) {
        this.theaterAminities = theaterAminities;
    }

    public void setShowTime(String showTime) {
        ShowTime = showTime;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getMovieDetails() {
        return movieDetails;
    }

    public void setMovieDetails(String movieDetails) {
        this.movieDetails = movieDetails;
    }

    public long getTheaterZip() {
        return theaterZip;
    }

    public void setTheaterZip(long theaterZip) {
        this.theaterZip = theaterZip;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}
