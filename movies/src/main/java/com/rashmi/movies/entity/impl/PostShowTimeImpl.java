package com.rashmi.movies.entity.impl;

import java.util.Date;

import com.rashmi.movies.entity.PostShowTime;

public class PostShowTimeImpl implements PostShowTime {
    private String movieName;
    private String theaterName;
    private String ShowTime;
    private long price;
    private String status;
    private Date fromDate;
    private Date toDate;

    public PostShowTimeImpl() {
    }

    public String getMovieName() {
        return movieName;
    }

    public String getTheaterName() {
        return theaterName;
    }

    public String getShowTime() {
        return ShowTime;
    }

    public long getPrice() {
        return price;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public void setTheaterName(String theaterName) {
        this.theaterName = theaterName;
    }

    public void setShowTime(String showTime) {
        ShowTime = showTime;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setFromDate(Date inputFromDate) {
        this.fromDate = inputFromDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}
