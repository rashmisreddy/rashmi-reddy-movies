package com.rashmi.movies.entity;

import java.util.List;

public interface Theater {
    long getTheaterId();

    String getTheaterName();

    String getTheaterName(long addedTheaterId);

    String getTheaterCity();

    String getTheaterState();

    long getTheaterZip();

    String getPhone();

    String getTheaterAminities();

    List<Movie> getMovies();

    void addMovies(Movie movies);

    void setMovies(List<Movie> movies);
}