package com.rashmi.movies.entity.impl;

import java.util.Date;

import com.rashmi.movies.entity.PostBuyTicket;

public class PostBuyTicketImpl implements PostBuyTicket {
    private String movieName;
    private String showTime;
    private String theaterName;
    private String emailId;
    private String status;
    private long noOfTickets;
    private Date date;

    public String getMovieName() {
        return movieName;
    }

    public String getTheaterName() {
        return theaterName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public void setTheaterName(String theaterName) {
        this.theaterName = theaterName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getNoOfTickets() {
        return noOfTickets;
    }

    public void setNoOfTickets(long noOfTickets) {
        this.noOfTickets = noOfTickets;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getShowTime() {
        return showTime;
    }

    public void setShowTime(String showTime) {
        this.showTime = showTime;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
