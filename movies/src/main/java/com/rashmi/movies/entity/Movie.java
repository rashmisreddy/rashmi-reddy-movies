package com.rashmi.movies.entity;

import java.util.List;

public interface Movie {
    long getMovieId();

    String getMovieName(long movieid);

    String getMovieDetails(long movieid);

    public List<Theater> getTheaters();

    public void setTheaters(List<Theater> theaters);

    public void addTheater(Theater theater);

    public String getMovieName();

    public String getMovieDetails();
}