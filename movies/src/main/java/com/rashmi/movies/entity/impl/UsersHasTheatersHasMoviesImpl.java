package com.rashmi.movies.entity.impl;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.rashmi.movies.entity.UsersHasTheatersHasMovies;

@Entity
@Table(name = "Users_has_Theaters_has_Movies")
public class UsersHasTheatersHasMoviesImpl
        implements UsersHasTheatersHasMovies {
    @Id
    @Column(name = "txn_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long txnid;

    @Column(name = "Users_user_id")
    private long userid;

    @Column(name = "Theaters_has_Movies_id")
    private long theatershasmoviesid;

    @Column(name = "No_of_tickets")
    private long nooftickets;

    @Column(name = "total_price")
    private long totalprice;

    @Column(name = "status")
    private String status;

    @Column(name = "date")
    private Date date;

    public UsersHasTheatersHasMoviesImpl() {
    }

    public UsersHasTheatersHasMoviesImpl(long txnid) {
        this.txnid = txnid;
    }

    public long getTxnid() {
        return txnid;
    }

    public long getUserid() {
        return userid;
    }

    public long getTheatershasmoviesid() {
        return theatershasmoviesid;
    }

    public long getNooftickets() {
        return nooftickets;
    }

    public long getTotalprice() {
        return totalprice;
    }

    public void setTxnid(long txnid) {
        this.txnid = txnid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public void setTheatershasmoviesid(long theatershasmoviesid) {
        this.theatershasmoviesid = theatershasmoviesid;
    }

    public void setNooftickets(long nooftickets) {
        this.nooftickets = nooftickets;
    }

    public void setTotalprice(long totalprice) {
        this.totalprice = totalprice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
