package com.rashmi.movies.entity;

import java.util.Date;

public interface GetBuyTicket {
    String getMovieName();

    String getTheaterName();

    void setMovieName(String movieName);

    void setTheaterName(String theaterName);

    long getNooftickets();

    void setNooftickets(long nooftickets);

    String getStatus();

    void setStatus(String status);

    public long getTotalPrice();

    public void setTotalPrice(long totalPrice);

    public String getEmailId();

    public void setEmailId(String emailId);

    long getTxnid();

    void setTxnid(long txnid);

    String getFirstName();

    String getLastName();

    void setFirstName(String firstName);

    void setLastName(String lastName);

    String getShowTime();

    void setShowTime(String showTime);

    Date getDate();

    void setDate(Date date);
}
