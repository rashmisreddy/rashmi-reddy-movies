package com.rashmi.movies.entity.impl;

import java.util.Date;

import com.rashmi.movies.entity.PutShowTime;

public class PutShowTimeImpl implements PutShowTime {
    private String ShowTime;
    private long price;
    private String status;
    private Date fromDate;
    private Date toDate;

    public String getShowTime() {
        return ShowTime;
    }

    public long getPrice() {
        return price;
    }

    public String getStatus() {
        return status;
    }

    public void setShowTime(String showTime) {
        ShowTime = showTime;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}
