package com.rashmi.movies.entity;

public interface User {
    long getUserId();

    String getFirstName();

    String getLastName();

    String getEmailId();

    long getPhone();

    String getPassword();

    String getPaymentType();
    
    Address getAddress();
    
    void addListOfTheatersHasMovies(TheatersHasMovies theatersHasMovies);
}