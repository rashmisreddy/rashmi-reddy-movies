package com.rashmi.movies.entity.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.cfg.ImprovedNamingStrategy;

import com.rashmi.movies.entity.Address;
import com.rashmi.movies.entity.TheatersHasMovies;
import com.rashmi.movies.entity.User;

@Entity
@Table(name = "Users")
public class UserImpl extends ImprovedNamingStrategy implements User {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;
    
    @Column(name = "first_name")
    private String firstName;
    
    @Column(name = "last_name")
    private String lastName;
    
    @Column(name = "emailid")
    private String emailId;
    
    @Column(name = "phone")
    private long Phone;
    
    @Column(name = "password")
    private String Password;
    
    @Column(name = "payment_type")
    private String paymentType;

    @OneToOne(targetEntity = AddressImpl.class, optional = true, cascade = CascadeType.ALL, mappedBy = "user")
    @Cascade({ org.hibernate.annotations.CascadeType.ALL })
    private Address address;

    @ManyToMany(targetEntity = TheatersHasMoviesImpl.class, fetch = FetchType.EAGER)
    @JoinTable(name = "Users_has_Theaters_has_Movies", joinColumns = {
            @JoinColumn(name = "Users_user_id", nullable = false) }, inverseJoinColumns = {
                    @JoinColumn(name = "Theaters_has_Movies_id", nullable = false) })
    List<TheatersHasMovies> listOfTheatersHasMovies = new ArrayList<TheatersHasMovies>();

    public UserImpl() {
    }

    public UserImpl(long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getPassword() {
        return Password;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public long getUserId() {
        return this.userId;
    }

    public long getPhone() {
        return this.Phone;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setPhone(long phone) {
        Phone = phone;
    }

    public Address getAddress() {
        return this.address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<TheatersHasMovies> getListOfTheatersHasMovies() {
        return listOfTheatersHasMovies;
    }

    public void setListOfTheatersHasMovies(
            List<TheatersHasMovies> listOfTheatersHasMovies) {
        this.listOfTheatersHasMovies = listOfTheatersHasMovies;
    }

    public void addListOfTheatersHasMovies(
            TheatersHasMovies theatersHasMovies) {
        if (listOfTheatersHasMovies == null) {
            listOfTheatersHasMovies = new ArrayList<TheatersHasMovies>();
        }
        listOfTheatersHasMovies.add(theatersHasMovies);
    }
}