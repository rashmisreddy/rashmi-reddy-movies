package com.rashmi.movies.entity;

import java.util.Date;

public interface UsersHasTheatersHasMovies {
    long getTxnid();

    long getUserid();

    long getTheatershasmoviesid();

    long getNooftickets();

    long getTotalprice();

    void setTxnid(long txnid);

    void setUserid(long userid);

    void setTheatershasmoviesid(long theatershasmoviesid);

    void setNooftickets(long nooftickets);

    void setTotalprice(long totalprice);

    public String getStatus();

    void setStatus(String status);

    Date getDate();

    void setDate(Date date);
}
