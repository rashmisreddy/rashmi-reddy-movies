package com.rashmi.movies.entity;

import java.util.Date;

public interface TheatersHasMovies {

    String getShowtime();

    long getPrice();

    void setShowtime(String showtime);

    void setPrice(long price);

    long getId();

    void setId(long id);

    long getMovieid();

    long getTheaterid();

    void setMovieid(long movieid);

    void setTheaterid(long theaterid);

    String getStatus();

    void setStatus(String status);

    void setFromDate(Date fromDate);

    void setToDate(Date toDate);

    Date getFromDate();

    Date getToDate();
}
