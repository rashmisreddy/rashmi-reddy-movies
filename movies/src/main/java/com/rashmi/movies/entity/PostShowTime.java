package com.rashmi.movies.entity;

import java.util.Date;

public interface PostShowTime {
    String getMovieName();

    String getTheaterName();

    String getShowTime();

    long getPrice();

    void setMovieName(String movieName);

    void setTheaterName(String theaterName);

    void setShowTime(String showTime);

    void setPrice(long price);

    String getStatus();

    void setStatus(String status);

    Date getFromDate();

    Date getToDate();

    void setFromDate(Date fromDate);

    void setToDate(Date toDate);
}
