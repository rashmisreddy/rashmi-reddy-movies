package com.rashmi.movies.entity;

import java.util.Date;

public interface PutShowTime {
    String getShowTime();

    long getPrice();

    void setShowTime(String showTime);

    void setPrice(long price);

    String getStatus();

    void setStatus(String status);

    Date getFromDate();

    Date getToDate();

    void setFromDate(Date fromDate);

    void setToDate(Date toDate);
}
