package com.rashmi.movies.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.rashmi.movies.entity.GetShowTime;
import com.rashmi.movies.entity.Movie;
import com.rashmi.movies.entity.PostShowTime;
import com.rashmi.movies.entity.Theater;
import com.rashmi.movies.entity.TheatersHasMovies;
import com.rashmi.movies.entity.impl.GetShowTimeImpl;
import com.rashmi.movies.entity.impl.TheatersHasMoviesImpl;
import com.rashmi.movies.repository.TheatersHasMoviesRepository;
import com.rashmi.movies.service.exception.MovieException;

@Repository
public class TheatersHasMoviesRepositoryImpl
        implements TheatersHasMoviesRepository {

    @Autowired
    private SessionFactory sessionFactory;

    final String STATUS_ACTIVE = "ACTIVE";
    final String STATUS_RUNNING = "RUNNING";

    @Override
    public GetShowTime updateShowTime(PostShowTime putShowTime) {
        Criteria crit1 = this.sessionFactory.getCurrentSession()
                .createCriteria(Movie.class);

        if (!StringUtils.isEmpty(putShowTime.getMovieName())) {
            crit1.add(
                    Restrictions.like("movieName", putShowTime.getMovieName()));
        }
        Movie searchMovieResult = (Movie) crit1.uniqueResult();

        Criteria crit2 = this.sessionFactory.getCurrentSession()
                .createCriteria(Theater.class);

        if (!StringUtils.isEmpty(putShowTime.getTheaterName())) {
            crit2.add(Restrictions.like("theaterName",
                    putShowTime.getTheaterName()));
        }
        Theater searchTheaterResult = (Theater) crit2.uniqueResult();

        Criteria crit = this.sessionFactory.getCurrentSession()
                .createCriteria(TheatersHasMovies.class);

        crit.add(Restrictions.and(Restrictions.eq("theaterid",
                searchTheaterResult.getTheaterId())));
        crit.add(Restrictions.and(
                Restrictions.eq("movieid", searchMovieResult.getMovieId())));
        crit.add(Restrictions
                .and(Restrictions.like("showtime", putShowTime.getShowTime())));
        crit.add(Restrictions
                .and(Restrictions.like("fromDate", putShowTime.getFromDate())));
        crit.add(Restrictions
                .and(Restrictions.like("toDate", putShowTime.getToDate())));
        crit.add(Restrictions
                .and(Restrictions.like("status", putShowTime.getStatus())));

        TheatersHasMovies fetchTheatersHasMovies = (TheatersHasMovies) crit
                .uniqueResult();

        GetShowTimeImpl getShowTime = new GetShowTimeImpl();

        if (fetchTheatersHasMovies != null) {
            if (!(putShowTime.getShowTime()
                    .equals(fetchTheatersHasMovies.getShowtime())))
                fetchTheatersHasMovies.setShowtime(putShowTime.getShowTime());

            if (putShowTime.getPrice() != fetchTheatersHasMovies.getPrice())
                fetchTheatersHasMovies.setPrice(putShowTime.getPrice());

            if (!(putShowTime.getStatus()
                    .equals(fetchTheatersHasMovies.getStatus())))
                fetchTheatersHasMovies.setStatus(putShowTime.getStatus());

            fetchTheatersHasMovies.setStatus(STATUS_RUNNING);
            this.sessionFactory.getCurrentSession()
                    .update(fetchTheatersHasMovies);

            getShowTime.setId((int) fetchTheatersHasMovies.getId());
            getShowTime.setFromDate(fetchTheatersHasMovies.getFromDate());
            getShowTime.setMovieDetails(searchMovieResult.getMovieDetails());
            getShowTime.setMovieName(searchMovieResult.getMovieName());
            getShowTime.setPrice(fetchTheatersHasMovies.getPrice());
            getShowTime.setShowTime(fetchTheatersHasMovies.getShowtime());
            getShowTime.setStatus(fetchTheatersHasMovies.getStatus());
            getShowTime.setTheaterAminities(
                    searchTheaterResult.getTheaterAminities());
            getShowTime.setTheaterCity(searchTheaterResult.getTheaterCity());
            getShowTime.setTheaterName(searchTheaterResult.getTheaterName());
            getShowTime.setTheaterPhone(searchTheaterResult.getPhone());
            getShowTime.setTheaterState(searchTheaterResult.getTheaterState());
            getShowTime.setTheaterZip(searchTheaterResult.getTheaterZip());
            getShowTime.setToDate(fetchTheatersHasMovies.getToDate());
        }
        return (GetShowTime) getShowTime;
    }

    @Transactional // at method level
    public GetShowTime addShowtime(PostShowTime newPostShowTime)
            throws MovieException {
        Criteria crit1 = this.sessionFactory.getCurrentSession()
                .createCriteria(Movie.class);

        if (!StringUtils.isEmpty(newPostShowTime.getMovieName())) {
            crit1.add(Restrictions.like("movieName",
                    newPostShowTime.getMovieName()));
        }
        Movie searchMovieResult = (Movie) crit1.uniqueResult();

        Criteria crit2 = this.sessionFactory.getCurrentSession()
                .createCriteria(Theater.class);

        if (!StringUtils.isEmpty(newPostShowTime.getTheaterName())) {
            crit2.add(Restrictions.like("theaterName",
                    newPostShowTime.getTheaterName()));
        }
        Theater searchTheaterResult = (Theater) crit2.uniqueResult();

        Criteria crit = this.sessionFactory.getCurrentSession()
                .createCriteria(TheatersHasMovies.class);

        crit.add(Restrictions.and(Restrictions.eq("theaterid",
                searchTheaterResult.getTheaterId())));
        crit.add(Restrictions.and(
                Restrictions.eq("movieid", searchMovieResult.getMovieId())));
        crit.add(Restrictions.and(
                Restrictions.like("showtime", newPostShowTime.getShowTime())));
        crit.add(Restrictions.and(
                Restrictions.like("fromDate", newPostShowTime.getFromDate())));
        crit.add(Restrictions
                .and(Restrictions.like("toDate", newPostShowTime.getToDate())));
        crit.add(Restrictions
                .and(Restrictions.like("status", newPostShowTime.getStatus())));

        TheatersHasMovies searchTheatersHasMoviesResult = (TheatersHasMovies) crit
                .uniqueResult();

        GetShowTimeImpl getShowTime = new GetShowTimeImpl();

        if (searchTheatersHasMoviesResult == null) {
            TheatersHasMoviesImpl newTheatersHasMovies = new TheatersHasMoviesImpl();

            newTheatersHasMovies
                    .setTheaterid(searchTheaterResult.getTheaterId());
            newTheatersHasMovies.setMovieid(searchMovieResult.getMovieId());
            newTheatersHasMovies.setFromDate(newPostShowTime.getFromDate());
            newTheatersHasMovies.setToDate(newPostShowTime.getToDate());
            newTheatersHasMovies.setShowtime(newPostShowTime.getShowTime());
            newTheatersHasMovies.setPrice(newPostShowTime.getPrice());
            newTheatersHasMovies.setStatus(STATUS_ACTIVE);

            long addedTxnId = (long) this.sessionFactory.getCurrentSession()
                    .save((TheatersHasMovies) newTheatersHasMovies);

            TheatersHasMovies fetchTheatersHasMovies = getTheatersHasMovies(
                    addedTxnId);

            getShowTime.setId((int) fetchTheatersHasMovies.getId());
            getShowTime.setFromDate(fetchTheatersHasMovies.getFromDate());
            getShowTime.setMovieDetails(searchMovieResult.getMovieDetails());
            getShowTime.setMovieName(searchMovieResult.getMovieName());
            getShowTime.setPrice(fetchTheatersHasMovies.getPrice());
            getShowTime.setShowTime(fetchTheatersHasMovies.getShowtime());
            getShowTime.setStatus(fetchTheatersHasMovies.getStatus());
            getShowTime.setTheaterAminities(
                    searchTheaterResult.getTheaterAminities());
            getShowTime.setTheaterCity(searchTheaterResult.getTheaterCity());
            getShowTime.setTheaterName(searchTheaterResult.getTheaterName());
            getShowTime.setTheaterPhone(searchTheaterResult.getPhone());
            getShowTime.setTheaterState(searchTheaterResult.getTheaterState());
            getShowTime.setTheaterZip(searchTheaterResult.getTheaterZip());
            getShowTime.setToDate(fetchTheatersHasMovies.getToDate());

        }
        return getShowTime;

    }

    public TheatersHasMovies getTheatersHasMovies(long id) {
        return (TheatersHasMovies) this.sessionFactory.getCurrentSession()
                .get(TheatersHasMoviesImpl.class, id);
    }

    public long getShowTimes(PostShowTime showTime) {

        Criteria crit1 = this.sessionFactory.getCurrentSession()
                .createCriteria(Movie.class);

        if (!StringUtils.isEmpty(showTime.getMovieName())) {
            crit1.add(Restrictions.like("movieName", showTime.getMovieName()));
        }
        Movie searchMovieResult = (Movie) crit1.uniqueResult();

        Criteria crit2 = this.sessionFactory.getCurrentSession()
                .createCriteria(Theater.class);

        if (!StringUtils.isEmpty(showTime.getTheaterName())) {
            crit2.add(Restrictions.like("theaterName",
                    showTime.getTheaterName()));
        }
        Theater searchTheaterResult = (Theater) crit2.uniqueResult();

        Criteria crit = this.sessionFactory.getCurrentSession()
                .createCriteria(TheatersHasMovies.class);

        crit.add(Restrictions.and(Restrictions.eq("theaterid",
                searchTheaterResult.getTheaterId())));
        crit.add(Restrictions.and(
                Restrictions.eq("movieid", searchMovieResult.getMovieId())));
        crit.add(Restrictions
                .and(Restrictions.like("showtime", showTime.getShowTime())));
        crit.add(Restrictions
                .and(Restrictions.like("fromDate", showTime.getFromDate())));
        crit.add(Restrictions
                .and(Restrictions.like("toDate", showTime.getToDate())));
        crit.add(Restrictions
                .and(Restrictions.like("status", showTime.getStatus())));

        TheatersHasMovies searchTheatersHasMoviesResult = (TheatersHasMovies) crit
                .uniqueResult();

        return searchTheatersHasMoviesResult.getId();
    }

    @SuppressWarnings("unchecked")
    public List<GetShowTime> getShowtimeByMovieName(String movieName) {
        List<GetShowTime> showtimeResultList = new ArrayList<GetShowTime>();

        Criteria crit1 = this.sessionFactory.getCurrentSession()
                .createCriteria(Movie.class);

        if (!StringUtils.isEmpty(movieName)) {
            crit1.add(Restrictions.like("movieName", movieName));
        }
        Movie searchMovieResult = (Movie) crit1.uniqueResult();

        Criteria crit = this.sessionFactory.getCurrentSession()
                .createCriteria(TheatersHasMovies.class);

        crit.add(Restrictions.and(
                Restrictions.eq("movieid", searchMovieResult.getMovieId())))
                .add(Restrictions.like("status", "RUNNING"));

        List<TheatersHasMovies> searchTheatersHasMoviesResult = crit.list();

        for (TheatersHasMovies result : searchTheatersHasMoviesResult) {
            GetShowTimeImpl showtime = new GetShowTimeImpl();
            showtime.setMovieName(searchMovieResult.getMovieName());
            showtime.setMovieDetails(searchMovieResult.getMovieDetails());

            Criteria crit2 = this.sessionFactory.getCurrentSession()
                    .createCriteria(Theater.class);

            crit2.add(Restrictions
                    .and(Restrictions.eq("theaterId", result.getTheaterid())));

            Theater searchTheaterResult = (Theater) crit2.uniqueResult();

            showtime.setId((int) result.getId());
            showtime.setTheaterName(searchTheaterResult.getTheaterName());
            showtime.setTheaterPhone(searchTheaterResult.getPhone());
            showtime.setTheaterCity(searchTheaterResult.getTheaterCity());
            showtime.setTheaterState(searchTheaterResult.getTheaterState());
            showtime.setTheaterZip(searchTheaterResult.getTheaterZip());
            showtime.setTheaterAminities(
                    searchTheaterResult.getTheaterAminities());

            showtime.setShowTime(result.getShowtime());
            showtime.setPrice(result.getPrice());
            showtime.setStatus(result.getStatus());
            showtime.setFromDate(result.getFromDate());
            showtime.setToDate(result.getToDate());

            showtimeResultList.add(showtime);

        }
        return showtimeResultList;
    }

    @SuppressWarnings("unchecked")
    public List<GetShowTime> getShowtimeByTheaterName(String theaterName) {
        List<GetShowTime> showtimeResultList = new ArrayList<GetShowTime>();

        Criteria crit1 = this.sessionFactory.getCurrentSession()
                .createCriteria(Theater.class);

        if (!StringUtils.isEmpty(theaterName)) {
            crit1.add(Restrictions.like("theaterName", theaterName));
        }
        Theater searchTheaterResult = (Theater) crit1.uniqueResult();

        if (searchTheaterResult != null) {
            Criteria crit = this.sessionFactory.getCurrentSession()
                    .createCriteria(TheatersHasMovies.class);

            crit.add(Restrictions.and(Restrictions.eq("theaterid",
                    searchTheaterResult.getTheaterId())))
                    .add(Restrictions.like("status", "RUNNING"));

            List<TheatersHasMovies> searchTheatersHasMoviesResult = crit.list();

            for (TheatersHasMovies result : searchTheatersHasMoviesResult) {
                GetShowTimeImpl showtime = new GetShowTimeImpl();

                Criteria crit2 = this.sessionFactory.getCurrentSession()
                        .createCriteria(Movie.class);

                crit2.add(Restrictions
                        .and(Restrictions.eq("movieId", result.getMovieid())));

                Movie searchMovieResult = (Movie) crit2.uniqueResult();

                showtime.setId((int) result.getId());

                showtime.setMovieName(searchMovieResult.getMovieName());
                showtime.setMovieDetails(searchMovieResult.getMovieDetails());

                showtime.setTheaterName(searchTheaterResult.getTheaterName());
                showtime.setTheaterPhone(searchTheaterResult.getPhone());
                showtime.setTheaterCity(searchTheaterResult.getTheaterCity());
                showtime.setTheaterState(searchTheaterResult.getTheaterState());
                showtime.setTheaterZip(searchTheaterResult.getTheaterZip());
                showtime.setTheaterAminities(
                        searchTheaterResult.getTheaterAminities());

                showtime.setShowTime(result.getShowtime());
                showtime.setPrice(result.getPrice());
                showtime.setStatus(result.getStatus());
                showtime.setFromDate(result.getFromDate());
                showtime.setToDate(result.getToDate());

                showtimeResultList.add(showtime);
            }
        }
        return showtimeResultList;
    }
}