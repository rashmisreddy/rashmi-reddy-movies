package com.rashmi.movies.repository;

import com.rashmi.movies.entity.GetBuyTicket;
import com.rashmi.movies.entity.PostBuyTicket;
import com.rashmi.movies.entity.UsersHasTheatersHasMovies;

public interface UsersHasTheatersHasMoviesRepository {
    GetBuyTicket addTxn(PostBuyTicket newPostBuyTicket);

    UsersHasTheatersHasMovies getTxn(long txnId);

    GetBuyTicket getTransaction(long txnId);

    void updateTxn(UsersHasTheatersHasMovies usersHasTheatersHasMovies);

}
