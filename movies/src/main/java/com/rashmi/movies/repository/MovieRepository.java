package com.rashmi.movies.repository;

import java.util.List;

import com.rashmi.movies.entity.Movie;

public interface MovieRepository {
    /**
     * 
     * @param Movie
     * @return the id of the newly added movie
     */
    long addMovie(Movie movie);

    Movie getMovie(long movieId);

    List<Movie> getAllMovies();

    List<Movie> search(String movieName);

    void update(Movie movie);
    
    void deleteMovie(Movie movie);
}
