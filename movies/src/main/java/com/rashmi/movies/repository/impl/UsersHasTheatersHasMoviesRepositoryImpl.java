package com.rashmi.movies.repository.impl;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.rashmi.movies.entity.GetBuyTicket;
import com.rashmi.movies.entity.Movie;
import com.rashmi.movies.entity.PostBuyTicket;
import com.rashmi.movies.entity.Theater;
import com.rashmi.movies.entity.TheatersHasMovies;
import com.rashmi.movies.entity.User;
import com.rashmi.movies.entity.UsersHasTheatersHasMovies;
import com.rashmi.movies.entity.impl.GetBuyTicketImpl;
import com.rashmi.movies.entity.impl.UsersHasTheatersHasMoviesImpl;
import com.rashmi.movies.repository.UsersHasTheatersHasMoviesRepository;

@Repository
public class UsersHasTheatersHasMoviesRepositoryImpl
        implements UsersHasTheatersHasMoviesRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public GetBuyTicket addTxn(PostBuyTicket newPostBuyTicket) {
        // Get User details from emailId
        Criteria crit = this.sessionFactory.getCurrentSession()
                .createCriteria(User.class);

        crit.add(Restrictions.eq("emailId", newPostBuyTicket.getEmailId()));
        User searchUserResult = (User) crit.uniqueResult();

        // Get Movie from movieName
        Criteria crit1 = this.sessionFactory.getCurrentSession()
                .createCriteria(Movie.class);

        crit1.add(
                Restrictions.eq("movieName", newPostBuyTicket.getMovieName()));
        Movie searchMovieResult = (Movie) crit1.uniqueResult();

        // Get Theater from theaterName
        Criteria crit2 = this.sessionFactory.getCurrentSession()
                .createCriteria(Theater.class);

        crit2.add(Restrictions.eq("theaterName",
                newPostBuyTicket.getTheaterName()));

        Theater searchTheaterResult = (Theater) crit2.uniqueResult();

        // Get id from TheaterHasMovies
        Criteria crit4 = this.sessionFactory.getCurrentSession()
                .createCriteria(TheatersHasMovies.class);

        System.out.println(">>>>>>>>>>>>>searchMovieResult.getMovieId() : "
                + searchMovieResult.getMovieId());
        System.out.println(">>>>>>>>>>>>>searchTheaterResult.getTheaterId() : "
                + searchTheaterResult.getTheaterId());
        System.out.println(">>>>>>>>>>>>>putShowTime.getToDate() : "
                + newPostBuyTicket.getDate());
        System.out.println(">>>>>>>>>>>>>putShowTime.getShowTime() : "
                + newPostBuyTicket.getShowTime());
        crit4.add(Restrictions.and(Restrictions.eq("theaterid",
                searchTheaterResult.getTheaterId())));
        crit4.add(Restrictions.and(
                Restrictions.eq("movieid", searchMovieResult.getMovieId())));
        crit4.add(Restrictions.and(
                Restrictions.like("showtime", newPostBuyTicket.getShowTime())));
        crit4.add(Restrictions
                .and(Restrictions.le("fromDate", newPostBuyTicket.getDate())));
        crit4.add(Restrictions
                .and(Restrictions.ge("toDate", newPostBuyTicket.getDate())));

        crit4.add(Restrictions.and(Restrictions.like("status", "RUNNING")));

        // crit4.add(Restrictions.eq("movieid", searchMovieResult.getMovieId()))
        // .add(Restrictions.eq("theaterid",
        // searchTheaterResult.getTheaterId()))
        // .add(Restrictions.eq("showtime",
        // newPostBuyTicket.getShowTime()))
        // .add(Restrictions.eq("status", "RUNNING"))
        // .add(Restrictions.eq("fromDate",
        // newPostBuyTicket.getFromDate()))
        // .add(Restrictions.eq("toDate", newPostBuyTicket.getToDate()));
        TheatersHasMovies searchTheatersHasMoviesResult = (TheatersHasMovies) crit4
                .uniqueResult();

        System.out.println(">>>>>>>>>>searchTheatersHasMoviesResult.getId() "
                + searchTheatersHasMoviesResult.getId());

        UsersHasTheatersHasMoviesImpl usersHasTheatersHasMoviesImpl = new UsersHasTheatersHasMoviesImpl();
        GetBuyTicketImpl returnAddedTxnDetails = new GetBuyTicketImpl();

        if (searchTheatersHasMoviesResult != null) {
            System.out.println(">>>>>>>>>");
            long price = searchTheatersHasMoviesResult.getPrice();

            usersHasTheatersHasMoviesImpl.setTheatershasmoviesid(
                    searchTheatersHasMoviesResult.getId());
            usersHasTheatersHasMoviesImpl
                    .setUserid(searchUserResult.getUserId());
            usersHasTheatersHasMoviesImpl
                    .setNooftickets(newPostBuyTicket.getNoOfTickets());
            usersHasTheatersHasMoviesImpl.setDate(newPostBuyTicket.getDate());
            usersHasTheatersHasMoviesImpl
                    .setTotalprice(newPostBuyTicket.getNoOfTickets() * price);
            usersHasTheatersHasMoviesImpl.setStatus("INITIATED");

            UsersHasTheatersHasMovies addedUsersHasTheatersHasMovies = getTxn(
                    (long) this.sessionFactory.getCurrentSession().save(
                            (UsersHasTheatersHasMovies) usersHasTheatersHasMoviesImpl));

            returnAddedTxnDetails
                    .setTxnid(addedUsersHasTheatersHasMovies.getTxnid());
            returnAddedTxnDetails
                    .setMovieName(searchMovieResult.getMovieName());
            returnAddedTxnDetails
                    .setTheaterName(searchTheaterResult.getTheaterName());
            returnAddedTxnDetails.setEmailId(searchUserResult.getEmailId());
            returnAddedTxnDetails.setNooftickets(
                    addedUsersHasTheatersHasMovies.getNooftickets());
            returnAddedTxnDetails.setTotalPrice(
                    addedUsersHasTheatersHasMovies.getTotalprice());
            returnAddedTxnDetails
                    .setStatus(addedUsersHasTheatersHasMovies.getStatus());
        }
        return ((GetBuyTicket) returnAddedTxnDetails);
    }

    public UsersHasTheatersHasMovies getTxn(long txnId) {
        return (UsersHasTheatersHasMovies) this.sessionFactory
                .getCurrentSession()
                .get(UsersHasTheatersHasMoviesImpl.class, txnId);
    }

    public GetBuyTicket getTransaction(long txnId) {
        GetBuyTicket getBuyTicketImpl = new GetBuyTicketImpl();

        UsersHasTheatersHasMoviesImpl usersHasTheatersHasMoviesImpl = (UsersHasTheatersHasMoviesImpl) this.sessionFactory
                .getCurrentSession()
                .get(UsersHasTheatersHasMoviesImpl.class, txnId);

        if (usersHasTheatersHasMoviesImpl != null) {
            // Get TheatersHasMovies Id
            Criteria crit = this.sessionFactory.getCurrentSession()
                    .createCriteria(TheatersHasMovies.class);

            crit.add(Restrictions.eq("id",
                    usersHasTheatersHasMoviesImpl.getTheatershasmoviesid()));
            TheatersHasMovies searchTheatersHasMoviesResult = (TheatersHasMovies) crit
                    .uniqueResult();

            // Get Movie from movieid
            Criteria crit1 = this.sessionFactory.getCurrentSession()
                    .createCriteria(Movie.class);

            crit1.add(Restrictions.eq("movieId",
                    searchTheatersHasMoviesResult.getMovieid()));
            Movie searchMovieResult = (Movie) crit1.uniqueResult();

            // Get Theater from theaterId
            Criteria crit2 = this.sessionFactory.getCurrentSession()
                    .createCriteria(Theater.class);

            crit2.add(Restrictions.eq("theaterId",
                    searchTheatersHasMoviesResult.getTheaterid()));
            Theater searchTheaterResult = (Theater) crit2.uniqueResult();

            // Get User details from userid
            Criteria crit3 = this.sessionFactory.getCurrentSession()
                    .createCriteria(User.class);

            crit3.add(Restrictions.eq("userId",
                    usersHasTheatersHasMoviesImpl.getUserid()));
            User searchUserResult = (User) crit3.uniqueResult();

            getBuyTicketImpl.setTxnid(usersHasTheatersHasMoviesImpl.getTxnid());
            getBuyTicketImpl.setEmailId(searchUserResult.getEmailId());
            getBuyTicketImpl.setFirstName(searchUserResult.getFirstName());
            getBuyTicketImpl.setLastName(searchUserResult.getLastName());
            getBuyTicketImpl
                    .setTheaterName(searchTheaterResult.getTheaterName());
            getBuyTicketImpl.setMovieName(searchMovieResult.getMovieName());
            getBuyTicketImpl.setNooftickets(
                    usersHasTheatersHasMoviesImpl.getNooftickets());
            getBuyTicketImpl.setDate(usersHasTheatersHasMoviesImpl.getDate());
            getBuyTicketImpl.setTotalPrice(
                    usersHasTheatersHasMoviesImpl.getTotalprice());
            getBuyTicketImpl
                    .setStatus(usersHasTheatersHasMoviesImpl.getStatus());
        }

        return (GetBuyTicket) getBuyTicketImpl;
    }

    @Override
    @Transactional
    public void updateTxn(UsersHasTheatersHasMovies usersHasTheatersHasMovies) {
        this.sessionFactory.getCurrentSession()
                .update(usersHasTheatersHasMovies);
    }
}
