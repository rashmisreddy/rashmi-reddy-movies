package com.rashmi.movies.repository;

import java.util.List;

import com.rashmi.movies.entity.Theater;

public interface TheaterRepository {
    /**
     * 
     * @param Theater
     * @return the id of the newly added theater
     */
    long addTheater(Theater theater);

    Theater getTheater(long theaterId);

    List<Theater> getAllTheaters();

    List<Theater> search(String theaterName);

    void update(Theater theater);
    
    void deleteTheater(Theater theater);
}
