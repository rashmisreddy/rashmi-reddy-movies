package com.rashmi.movies.repository.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.rashmi.movies.entity.Theater;
import com.rashmi.movies.entity.impl.TheaterImpl;
import com.rashmi.movies.repository.TheaterRepository;

@Repository
public class TheaterRepositoryImpl implements TheaterRepository {

    @Autowired
    private SessionFactory sessionFactory;

    public long addTheater(Theater theater) {
        return (long) this.sessionFactory.getCurrentSession().save(theater);

    }

    public void deleteTheater(Theater theater) {
        this.sessionFactory.getCurrentSession().delete(theater);
    }

    public Theater getTheater(long theaterId) {
        return (Theater) this.sessionFactory.getCurrentSession()
                .get(TheaterImpl.class, theaterId);
    }

    public void update(Theater theater) {
        this.sessionFactory.getCurrentSession().update(theater);
    }

    @SuppressWarnings("unchecked")
    public List<Theater> getAllTheaters() {
        Criteria crit = this.sessionFactory.getCurrentSession()
                .createCriteria(Theater.class);
        List<Theater> searchResult = crit.list();
        return searchResult;
    }

    @SuppressWarnings("unchecked")
    public List<Theater> search(String theaterName) {
        Criteria crit = this.sessionFactory.getCurrentSession()
                .createCriteria(Theater.class);
        if (!StringUtils.isEmpty(theaterName)) {
            crit.add(Restrictions.like("theaterName", "%" + theaterName + "%"));
        }
        List<Theater> searchResult = crit.list();
        return searchResult;
    }
}
