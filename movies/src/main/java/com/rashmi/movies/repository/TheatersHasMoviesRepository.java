package com.rashmi.movies.repository;

import java.util.List;

import com.rashmi.movies.entity.GetShowTime;
import com.rashmi.movies.entity.PostShowTime;
import com.rashmi.movies.entity.TheatersHasMovies;

public interface TheatersHasMoviesRepository {
    TheatersHasMovies getTheatersHasMovies(long id);

    GetShowTime addShowtime(PostShowTime postShowTime);

    GetShowTime updateShowTime(PostShowTime putShowTime);

    long getShowTimes(PostShowTime showTime);

    List<GetShowTime> getShowtimeByMovieName(String movieName);

    List<GetShowTime> getShowtimeByTheaterName(String theaterName);

    // List<GetShowTime> getShowtimeByStatus(String theaterName, String
    // movieName,
    // String status);
    //
    // List<GetShowTime> getShowtimeByShowTime(String theaterName,
    // String movieName, String showTime);

    // List<GetShowTime> getShowTimeForUpdateDelete(String theaterName,
    // String movieName, String showTime, String status);

    // void deleteTheaterHasMovies(TheatersHasMovies theaterHasMovies);

    // List<GetShowTime> getShowtimeByShowTimeStatus(String theaterName,
    // String movieName, String showTime, String status);
}
