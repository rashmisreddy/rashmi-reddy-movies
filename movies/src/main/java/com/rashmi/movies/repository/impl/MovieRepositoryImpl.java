package com.rashmi.movies.repository.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.rashmi.movies.entity.Movie;
import com.rashmi.movies.entity.User;
import com.rashmi.movies.entity.impl.MovieImpl;
import com.rashmi.movies.repository.MovieRepository;

@Repository
public class MovieRepositoryImpl implements MovieRepository {
    @Autowired
    private SessionFactory sessionFactory;

    public long addMovie(Movie movie) {
        return (long) this.sessionFactory.getCurrentSession().save(movie);
    }

    public void deleteMovie(Movie movie) {
        this.sessionFactory.getCurrentSession().delete(movie);
    }
    
    public Movie getMovie(long movieId) {
        return (Movie) this.sessionFactory.getCurrentSession()
                .get(MovieImpl.class, movieId);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Movie> getAllMovies() {
        Criteria crit = this.sessionFactory.getCurrentSession()
                .createCriteria(Movie.class);
        List<Movie> searchResult = crit.list();
        return searchResult;
    }

    @SuppressWarnings("unchecked")
    public List<Movie> search(String movieName) {
        Criteria crit = this.sessionFactory.getCurrentSession()
                .createCriteria(Movie.class);
        if (!StringUtils.isEmpty(movieName)) {
            crit.add(Restrictions.like("movieName", "%" + movieName + "%"));
        }
        List<Movie> searchResult = crit.list();
        return searchResult;
    }

    @SuppressWarnings("unchecked")
    public List<Movie> getMovie(String movieName) {
        Criteria crit = this.sessionFactory.getCurrentSession()
                .createCriteria(Movie.class);
        if (!StringUtils.isEmpty(movieName)) {
            crit.add(Restrictions.like("movieName", movieName));
        }
        List<Movie> searchResult = crit.list();
        return searchResult;
    }

    public void update(Movie movie) {
        this.sessionFactory.getCurrentSession().update(movie);
    }
}
