package com.rashmi.movies.repository.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import com.rashmi.movies.entity.User;
import com.rashmi.movies.entity.impl.UserImpl;
import com.rashmi.movies.repository.UserRepository;
import com.rashmi.movies.service.exception.MovieException;

@Repository
public class UserRepositoryImpl implements UserRepository {
    @Autowired
    private SessionFactory sessionFactory;

    public long addUser(User user)
            throws MySQLIntegrityConstraintViolationException {
        try {
            return (long) this.sessionFactory.getCurrentSession().save(user);
        } catch (MovieException e) {
            e.printStackTrace();
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.TABLE_CONSTRAINT,
                    "TABLE_CONSTRAINT violation");
        }
    }

    public void deleteUser(User user) {
        this.sessionFactory.getCurrentSession().delete(user);
    }

    public User getUser(long userId) {
        return (User) this.sessionFactory.getCurrentSession()
                .get(UserImpl.class, userId);
    }

    public User getUser(String emailId) {
        Criteria crit = this.sessionFactory.getCurrentSession()
                .createCriteria(User.class);
        if (!StringUtils.isEmpty(emailId)) {
            crit.add(Restrictions.like("emailId", "%" + emailId + "%"));
        }
        User searchResult = (User) crit.uniqueResult();

        return searchResult;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> search(String firstName, String lastName) {
        Criteria crit = this.sessionFactory.getCurrentSession()
                .createCriteria(User.class);
        if (!StringUtils.isEmpty(firstName)) {
            crit.add(Restrictions.like("firstName", "%" + firstName + "%"));
        }
        if (!StringUtils.isEmpty(lastName)) {
            crit.add(Restrictions.like("lastName", "%" + lastName + "%"));
        }
        List<User> searchResult = crit.list();
        return searchResult;
    }

    @Transactional
    public void update(User user) {
        this.sessionFactory.getCurrentSession().update(user);
    }
}
