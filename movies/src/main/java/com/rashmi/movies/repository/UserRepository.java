package com.rashmi.movies.repository;

import java.util.List;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import com.rashmi.movies.entity.User;
import com.rashmi.movies.service.exception.MovieException;

public interface UserRepository {
    /**
     * 
     * @param user
     * @return the id of the newly added user
     * @throws MySQLIntegrityConstraintViolationException 
     * @throws MovieException 
     */
    long addUser(User user) throws MovieException, MySQLIntegrityConstraintViolationException;

    User getUser(long userId);

    User getUser(String emailId);

    List<User> search(String firstName, String lastName);

    void update(User user);
    
    void deleteUser(User user);
}
