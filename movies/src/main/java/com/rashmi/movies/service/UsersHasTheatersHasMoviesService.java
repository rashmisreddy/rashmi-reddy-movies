package com.rashmi.movies.service;

import com.rashmi.movies.entity.GetBuyTicket;
import com.rashmi.movies.entity.PostBuyTicket;
import com.rashmi.movies.entity.UsersHasTheatersHasMovies;

public interface UsersHasTheatersHasMoviesService {
    UsersHasTheatersHasMovies getTxn(long txnId);

    GetBuyTicket addTxn(PostBuyTicket newPostBuyTicket);

    void updateTxn(long txnId, String status);

    GetBuyTicket getTicket(long txnId);
}
