package com.rashmi.movies.service;

import java.util.List;

import com.rashmi.movies.entity.User;

public interface UserService {
    User getUser(long userId);

    User getUser(String emailId);

    User addUser(User user);

    boolean isValidPassword(long userId, String emailId, String password);

    User updateUser(User user, long userId);

    List<User> getUsers(String firstName, String lastName);

    User deleteUser(long userId);
}
