package com.rashmi.movies.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.rashmi.movies.entity.Movie;
import com.rashmi.movies.entity.impl.MovieImpl;
import com.rashmi.movies.repository.MovieRepository;
import com.rashmi.movies.service.MovieService;
import com.rashmi.movies.service.exception.MovieException;

@Service
public class MovieServiceImpl implements MovieService {

    @SuppressWarnings("unused")
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MovieRepository movieRepository;

    @Transactional // at method level
    public Movie getMovie(long movieId) {
        if (movieId == 0L) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
                    "no movieId parameter provided");
        }
        return movieRepository.getMovie(movieId);
    }

    @Transactional // at method level
    public Movie addMovie(Movie newMovie) {
        if (newMovie == null) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "Movie object is not provided in body");
        }
        if (StringUtils.isEmpty(newMovie.getMovieName())
                || StringUtils.isEmpty(newMovie.getMovieDetails())) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "Movie object is incomplete in body");
        }
        long addedMovieId = movieRepository.addMovie(newMovie);
        return getMovie(addedMovieId);
    }

    // @Transactional // at method level
    // public Movie addMovie(Movie newMovie) {
    // if (newMovie != null) {
    // if (StringUtils.isEmpty(newMovie.getMovieName())
    // || StringUtils.isEmpty(newMovie.getMovieDetails())) {
    // throw new MovieException(
    // com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
    // "no content");
    // } else {
    // long addedMovieId = movieRepository.addMovie(newMovie);
    // return getMovie(addedMovieId);
    // }
    // } else {
    // throw new MovieException(
    // com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
    // "no content");
    // }
    // }

    @Transactional // at method level
    public Movie deleteMovie(long movieId) {
        if (movieId == 0L) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "moveiId is not provided");
        }

        Movie foundMovie = getMovie(movieId);
        movieRepository.deleteMovie(foundMovie);
        return foundMovie;
    }

    @Override
    @Transactional // at method level
    public List<Movie> getAllMovies() {
        return movieRepository.getAllMovies();
    }

    @Transactional
    public Movie updateMovie(Movie movie, long movieId) {
        // MovieImpl updateMovie = new MovieImpl();
        if (movie == null && movieId == 0L) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "Movie object is not provided in body");
        }

        if (StringUtils.isEmpty(movie.getMovieName())
                || StringUtils.isEmpty(movie.getMovieDetails())) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "Movie object is incomplete in body");
        }

        MovieImpl fetchMovie = (MovieImpl) getMovie(movieId);

        if (fetchMovie != null) {
            if (!movie.getMovieName().equals(fetchMovie.getMovieName()))
                fetchMovie.setMovieName(movie.getMovieName());

            if (!movie.getMovieDetails().equals(fetchMovie.getMovieDetails()))
                fetchMovie.setMovieDetails(movie.getMovieDetails());
        }
        movieRepository.update((Movie) fetchMovie);
        return getMovie(fetchMovie.getMovieId());
    }

    @Override
    @Transactional
    public List<Movie> searchMovies(String movieName) {
        List<Movie> returnList = new ArrayList<>();
        if (StringUtils.isEmpty(movieName)) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
                    "no movieName search parameter provided");
        } else {
            returnList = movieRepository.search(movieName);
        }
        return returnList;
    }
}
