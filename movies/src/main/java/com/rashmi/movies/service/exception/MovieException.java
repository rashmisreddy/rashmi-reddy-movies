package com.rashmi.movies.service.exception;

/**
 * Root of our exception heirarchy
 *
 */
@SuppressWarnings("serial")
public class MovieException extends RuntimeException {
    private ErrorCode errorCode;

    public MovieException(ErrorCode code, String message, Throwable throwable) {
        super(message, throwable);
        this.errorCode = code;
    }

    public MovieException(ErrorCode string, String message) {
        super(message);
        this.errorCode = string;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

}
