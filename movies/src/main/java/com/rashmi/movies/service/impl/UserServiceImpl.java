package com.rashmi.movies.service.impl;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import com.rashmi.movies.entity.User;
import com.rashmi.movies.entity.impl.UserImpl;
import com.rashmi.movies.repository.UserRepository;
import com.rashmi.movies.service.UserService;
import com.rashmi.movies.service.exception.InvalidFieldException;
import com.rashmi.movies.service.exception.MovieException;

@Service
public class UserServiceImpl implements UserService {
    private static final int MAX_NAME_LENGTH = 45;
    private static final int MAX_PASSWORD_LENGTH = 8;

    @SuppressWarnings("unused")
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional // at method level
    public User getUser(long userId) {
        if (userId == 0L) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "userId is not provided");
        }
        return userRepository.getUser(userId);
    }

    @Override
    @Transactional // at method level
    public User getUser(String emailId) {
        if (emailId == null) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
                    "emailId is not provided");
        }
        return userRepository.getUser(emailId);
    }

    @Transactional // at method level
    public User deleteUser(long userId) {

        if (userId == 0L) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
                    "userId is not provided");
        }

        User foundUser = getUser(userId);
        userRepository.deleteUser(foundUser);
        return foundUser;
    }

    @Transactional // at method level
    public boolean isValidPassword(long userId, String emailId,
            String password) {
        User user = userRepository.getUser(userId);

        if (user == null || password == null) {
            return false;
        }
        return user.getPassword() != null
                && user.getPassword().equals(password);
    }

    @Override
    @Transactional // at method level
    public User addUser(User user) {
        if (user == null) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "User object is not provided in body");
        }

        if (user.getPassword() == null) {
            // TODO what is our exception handling strategy?
            throw new IllegalArgumentException("Password is required");
        }

        if (StringUtils.isEmpty(user.getPassword())
                || user.getPassword().length() > MAX_PASSWORD_LENGTH) {
            throw new InvalidFieldException("Password is required");
        }

        if (StringUtils.isEmpty(user.getFirstName())
                || user.getFirstName().length() > MAX_NAME_LENGTH) {
            throw new InvalidFieldException("firstName is required");
        }

        if (StringUtils.isEmpty(user.getLastName())
                || user.getLastName().length() > MAX_NAME_LENGTH) {
            throw new InvalidFieldException("lastName is required");
        }

        long id = 0L;

        if (getUser(user.getEmailId()) != null) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.EXISTS,
                    "emailId already exists");
        } else {
            UserImpl impl = (UserImpl) user;
            // let us hash the password - movie project does basic MD5
            impl.setPassword(md5base64(user.getPassword()));

            try {
                id = userRepository.addUser(user);
            } catch (MySQLIntegrityConstraintViolationException e) {
                e.printStackTrace();
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.TABLE_CONSTRAINT,
                        "TABLE_CONSTRAINT violation");
            } catch (MovieException e) {
                e.printStackTrace();
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.TABLE_CONSTRAINT,
                        "TABLE_CONSTRAINT violation");
            }
            return getUser(id);
        }
    }

    private String md5base64(String pin) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digest = md.digest(pin.getBytes("UTF-8"));
            return Base64.encodeBase64String(digest);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            logger.error("failed to md5", e);
        }

        // TODO - this needs to be handled better throw new throw new
        throw new IllegalArgumentException("Server fail");
    }

    @Transactional
    public User updateUser(User user, long userId) {
        if (user == null && userId == 0L) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "User object is not provided in body");
        }

        if (StringUtils.isEmpty(user.getEmailId())
                || StringUtils.isEmpty(user.getFirstName())
                || StringUtils.isEmpty(user.getLastName())
                || StringUtils.isEmpty(user.getPassword())
                || StringUtils.isEmpty(user.getPaymentType())
                || StringUtils.isEmpty(user.getPhone())) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "User object is incomplete in body");
        }

        if (user.getAddress() == null) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "Address object is not provided in body");
        }

        if (StringUtils.isEmpty(user.getAddress().getStreet1())
                || StringUtils.isEmpty(user.getAddress().getStreet2())
                || StringUtils.isEmpty(user.getAddress().getCity())
                || StringUtils.isEmpty(user.getAddress().getState())
                || StringUtils.isEmpty(user.getAddress().getZip())
                || StringUtils.isEmpty(user.getAddress().getCountry())) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "Address object is incomplete in body");
        }

        UserImpl fetchUser = (UserImpl) getUser(userId);

        if (fetchUser != null) {
            if (!user.getFirstName().equals(fetchUser.getFirstName()))
                fetchUser.setFirstName(user.getFirstName());

            if (!user.getLastName().equals(fetchUser.getLastName()))
                fetchUser.setLastName(user.getLastName());

            if (!user.getPassword().equals(fetchUser.getPassword()))
                fetchUser.setPassword(user.getPassword());

            if (!user.getPaymentType().equals(fetchUser.getPaymentType()))
                fetchUser.setPaymentType(user.getPaymentType());

            if (!user.getEmailId().equals(fetchUser.getEmailId()))
                fetchUser.setEmailId(user.getEmailId());

            if (user.getPhone() != fetchUser.getPhone())
                fetchUser.setPhone(user.getPhone());

            userRepository.update((User) fetchUser);
        }
        return getUser(fetchUser.getUserId());
    }

    @Override
    @Transactional
    public List<User> getUsers(String firstName, String lastName) {
        List<User> returnList = new ArrayList<>();
        if (StringUtils.isEmpty(firstName) && StringUtils.isEmpty(lastName)) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
                    "no search parameter firstName/lastName is provided");
        } else {
            returnList = userRepository.search(firstName, lastName);
        }
        return returnList;
    }
}
