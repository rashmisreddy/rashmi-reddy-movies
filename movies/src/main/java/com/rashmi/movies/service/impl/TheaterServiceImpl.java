package com.rashmi.movies.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.rashmi.movies.entity.Theater;
import com.rashmi.movies.entity.impl.TheaterImpl;
import com.rashmi.movies.repository.TheaterRepository;
import com.rashmi.movies.service.TheaterService;
import com.rashmi.movies.service.exception.MovieException;

@Service
@Transactional
public class TheaterServiceImpl implements TheaterService {
    @SuppressWarnings("unused")
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TheaterRepository theaterRepository;

    public Theater getTheater(long theaterId) {
        if (theaterId == 0L) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "theaterId is not provided");
        }
        return theaterRepository.getTheater(theaterId);
    }

    @Transactional // at method level
    public Theater addTheater(Theater newTheater) {
        if (newTheater == null) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "Movie object is not provided in body");
        }

        if (StringUtils.isEmpty(newTheater.getTheaterName())
                || StringUtils.isEmpty(newTheater.getTheaterCity())
                || StringUtils.isEmpty(newTheater.getTheaterState())
                || StringUtils.isEmpty(newTheater.getTheaterZip())
                || StringUtils.isEmpty(newTheater.getPhone())
                || StringUtils.isEmpty(newTheater.getTheaterAminities())) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "theater object is incomplete in body");
        }
        long addedTheaterId = theaterRepository.addTheater(newTheater);
        return getTheater(addedTheaterId);
    }

    @Transactional // at method level
    public Theater deleteTheater(long theaterId) {
        if (theaterId == 0L) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "theaterId is not provided");
        }
        Theater foundTheater = getTheater(theaterId);
        theaterRepository.deleteTheater(foundTheater);
        return foundTheater;
    }

    @Transactional
    public Theater updateTheater(Theater theater, long theaterId) {
        if (theater == null && theaterId == 0L) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "Movie object is not provided in body");
        }

        if (StringUtils.isEmpty(theater.getTheaterName())
                || StringUtils.isEmpty(theater.getTheaterCity())
                || StringUtils.isEmpty(theater.getTheaterState())
                || StringUtils.isEmpty(theater.getTheaterZip())
                || StringUtils.isEmpty(theater.getPhone())
                || StringUtils.isEmpty(theater.getTheaterAminities())) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "theater object is incomplete in body");
        }
        TheaterImpl fetchTheater = (TheaterImpl) getTheater(theaterId);

        if (fetchTheater != null) {
            if (!theater.getTheaterName().equals(fetchTheater.getTheaterName()))
                fetchTheater.setTheaterName(theater.getTheaterName());

            if (!theater.getTheaterCity().equals(fetchTheater.getTheaterCity()))
                fetchTheater.setTheaterCity(theater.getTheaterCity());

            if (!theater.getTheaterState()
                    .equals(fetchTheater.getTheaterState()))
                fetchTheater.setTheaterState(theater.getTheaterState());

            if (theater.getTheaterZip() != fetchTheater.getTheaterZip())
                fetchTheater.setTheaterZip(theater.getTheaterZip());

            if (theater.getPhone() != fetchTheater.getPhone())
                fetchTheater.setPhone(theater.getPhone());

            if (!theater.getTheaterAminities()
                    .equals(fetchTheater.getTheaterAminities()))
                fetchTheater.setTheaterAminities(theater.getTheaterAminities());
        }
        theaterRepository.update((Theater) fetchTheater);
        return getTheater(fetchTheater.getTheaterId());
    }

    @Override
    @Transactional
    public List<Theater> getTheater(String movieName) {
        List<Theater> returnList = new ArrayList<>();
        if (StringUtils.isEmpty(movieName)) {
            System.out.println("no search parameter movieName provided");
        } else {
            returnList = theaterRepository.search(movieName);
        }
        return returnList;
    }

    @Override
    public List<Theater> getAllTheaters() {
        return theaterRepository.getAllTheaters();
    }

    @Override
    @Transactional
    public List<Theater> searchTheater(String theaterName) {
        List<Theater> returnList = new ArrayList<>();
        if (StringUtils.isEmpty(theaterName)) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
                    "no search parameter theaterName provided");
        } else {
            returnList = theaterRepository.search(theaterName);
        }
        return returnList;
    }
}