package com.rashmi.movies.service;

import java.util.List;

import com.rashmi.movies.entity.Theater;

public interface TheaterService {
    Theater getTheater(long theaterId);

    Theater addTheater(Theater newTheater);

    Theater deleteTheater(long theaterId);

    List<Theater> getTheater(String movieName);

    List<Theater> getAllTheaters();

    Theater updateTheater(Theater theater, long theaterId);

    List<Theater> searchTheater(String theaterName);

}
