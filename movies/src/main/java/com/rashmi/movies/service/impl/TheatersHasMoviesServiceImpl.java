package com.rashmi.movies.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.rashmi.movies.entity.GetShowTime;
import com.rashmi.movies.entity.PostShowTime;
import com.rashmi.movies.entity.TheatersHasMovies;
import com.rashmi.movies.repository.TheatersHasMoviesRepository;
import com.rashmi.movies.service.TheatersHasMoviesService;
import com.rashmi.movies.service.exception.MovieException;

@Service
@Transactional
public class TheatersHasMoviesServiceImpl implements TheatersHasMoviesService {
    @SuppressWarnings("unused")
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TheatersHasMoviesRepository theatersHasMoviesRepository;

    final String STATUS_ACTIVE = "ACTIVE";
    final String STATUS_INACTIVE = "INACTIVE";

    @Transactional // at method level
    public GetShowTime addShowTimes(PostShowTime postShowTime)
            throws MovieException {
        if (postShowTime == null) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "ShowTime object is not provided in body");
        }

        if (postShowTime != null) {

            if (StringUtils.isEmpty(postShowTime.getMovieName())) {
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                        "movieName is required");
                // IllegalArgumentException("movieName is required");
            }
            if (StringUtils.isEmpty(postShowTime.getTheaterName())) {
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                        "theaterName is required");
            }
            if (postShowTime.getShowTime() == null) {
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                        "showtime is required");
            }
            if (postShowTime.getPrice() == 0L) {
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                        "price is required");
            }

            if (StringUtils.isEmpty(postShowTime.getFromDate())) {
                // TODO what is our exception handling strategy?
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                        "fromDate is required");
            }

            if (StringUtils.isEmpty(postShowTime.getToDate())) {
                // TODO what is our exception handling strategy?
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                        "toDate is required");
            }
        }
        postShowTime.setStatus(STATUS_ACTIVE);
        // long id = theatersHasMoviesRepository.addShowtime(postShowTime);
        return theatersHasMoviesRepository.addShowtime(postShowTime);
    }

    @Transactional // at method level
    public TheatersHasMovies getTheatersHasMovies(long id) {
        if (id == 0L) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
                    "id is not provided");
        }
        System.out.println(">>>>>>>>>>getTheatersHasMovies in : "
                + theatersHasMoviesRepository.getTheatersHasMovies(id));
        return theatersHasMoviesRepository.getTheatersHasMovies(id);
    }

    @Transactional // at method level
    public TheatersHasMovies getTheatersHasMovies(PostShowTime postShowTime) {
        if (postShowTime == null) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "ShowTime object is not provided in body");
        }
        return getTheatersHasMovies(
                theatersHasMoviesRepository.getShowTimes(postShowTime));
    }

    @Transactional
    public GetShowTime updateShowTimes(PostShowTime putShowTime) {
        // TheatersHasMoviesImpl fetchTheatersHasMovies =
        // (TheatersHasMoviesImpl) getTheatersHasMovies(
        // putShowTime);
        //
        // if (fetchTheatersHasMovies != null) {
        // if (!(putShowTime.getShowTime()
        // .equals(fetchTheatersHasMovies.getShowtime())))
        // fetchTheatersHasMovies.setShowtime(putShowTime.getShowTime());
        //
        // if (putShowTime.getPrice() != fetchTheatersHasMovies.getPrice())
        // fetchTheatersHasMovies.setPrice(putShowTime.getPrice());
        //
        // if (!(putShowTime.getStatus()
        // .equals(fetchTheatersHasMovies.getStatus())))
        // fetchTheatersHasMovies.setStatus(putShowTime.getStatus());
        //
        // fetchTheatersHasMovies.setStatus(STATUS_RUNNING);
        ;
        // }

        // System.out.println(">>>>>>>>before : "
        // + theatersHasMoviesRepository.updateShowTime(putShowTime));
        //
        // long returnTheatersHasMoviesRepository = theatersHasMoviesRepository
        // .updateShowTime(putShowTime);
        // System.out.println(">>>>>>>>returnTheatersHasMoviesRepository : "
        // + returnTheatersHasMoviesRepository);
        // long returnTheatersHasMoviesRepository = theatersHasMoviesRepository
        // .updateShowTime(putShowTime);
        // if (returnTheatersHasMoviesRepository == 0) {
        // System.out.println("In if");
        // TheatersHasMoviesImpl noDataTheatersHasMovies = new
        // TheatersHasMoviesImpl();
        //
        // noDataTheatersHasMovies.setId(0);
        // noDataTheatersHasMovies.setMovieid(0);
        // noDataTheatersHasMovies.setTheaterid(0);
        // noDataTheatersHasMovies.setFromDate("0000-00-00");
        // noDataTheatersHasMovies.setToDate("0000-00-00");
        // noDataTheatersHasMovies.setPrice(0);
        // noDataTheatersHasMovies.setShowtime("00:00");
        // noDataTheatersHasMovies.setStatus("NONE");
        //
        // return (GetShowTime) noDataTheatersHasMovies;
        // } else {
        // System.out.println("In else");
        //
        // System.out.println(">>>>>>>>after : "
        // + getTheatersHasMovies(returnTheatersHasMoviesRepository));

        // return getTheatersHasMovies(returnTheatersHasMoviesRepository);

        // }

        if (putShowTime == null) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "ShowTime object is not provided in body");
        }

        if (StringUtils.isEmpty(putShowTime.getMovieName())) {
            throw new IllegalArgumentException("movieName is required");
        }
        if (StringUtils.isEmpty(putShowTime.getTheaterName())) {
            throw new IllegalArgumentException("theaterName is required");
        }
        if (putShowTime.getShowTime() == null) {
            throw new IllegalArgumentException("showtime is required");
        }
        if (putShowTime.getPrice() == 0L) {
            throw new IllegalArgumentException("price is required");
        }
        if (StringUtils.isEmpty(putShowTime.getFromDate())) {
            // TODO what is our exception handling strategy?
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "fromDate is required");
        }

        if (StringUtils.isEmpty(putShowTime.getToDate())) {
            // TODO what is our exception handling strategy?
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "toDate is required");
        }

        System.out.println(">>>>>>>>>>>>>putShowTime.getFromDate() : "
                + putShowTime.getFromDate());
        System.out.println(">>>>>>>>>>>>>putShowTime.getToDate() : "
                + putShowTime.getToDate());
        System.out.println(">>>>>>>>>>>>>putShowTime.getShowTime() : "
                + putShowTime.getShowTime());
        System.out.println(">>>>>>>>>>>>>putShowTime.getStatus() : "
                + putShowTime.getStatus());
        System.out.println(">>>>>>i service layer : " + putShowTime.toString());
        return theatersHasMoviesRepository.updateShowTime(putShowTime);
    }

    public List<GetShowTime> getShowTimesByMovieName(String movieName) {
        List<GetShowTime> returnList = null;
        if (StringUtils.isEmpty(movieName)) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
                    "no search parameter for movieName is provided");
        } else {
            returnList = theatersHasMoviesRepository
                    .getShowtimeByMovieName(movieName);
        }
        return returnList;
    }

    public List<GetShowTime> getShowTimesByTheaterName(String theaterName) {
        List<GetShowTime> returnList = null;
        if (StringUtils.isEmpty(theaterName)) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
                    "no search parameter for theaterName is provided");
        } else {
            returnList = theatersHasMoviesRepository
                    .getShowtimeByTheaterName(theaterName);
        }
        return returnList;
    }
}