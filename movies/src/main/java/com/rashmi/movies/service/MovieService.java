package com.rashmi.movies.service;

import java.util.List;

import com.rashmi.movies.entity.Movie;

public interface MovieService {
    Movie getMovie(long movieId);

    Movie addMovie(Movie newMovie);

    List<Movie> getAllMovies();

    Movie updateMovie(Movie movie, long movieId);

    List<Movie> searchMovies(String movieName);

    Movie deleteMovie(long movieId);
}
