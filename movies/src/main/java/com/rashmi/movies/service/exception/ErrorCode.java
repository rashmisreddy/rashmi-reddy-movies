package com.rashmi.movies.service.exception;

public enum ErrorCode {
	INVALID_FIELD,
	MISSING_DATA,
	NO_CONTENT,
	TABLE_CONSTRAINT,
	EXISTS,
	DATE
}
