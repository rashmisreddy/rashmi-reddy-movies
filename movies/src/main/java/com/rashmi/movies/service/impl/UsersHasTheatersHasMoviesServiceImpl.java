package com.rashmi.movies.service.impl;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.rashmi.movies.entity.GetBuyTicket;
import com.rashmi.movies.entity.PostBuyTicket;
import com.rashmi.movies.entity.UsersHasTheatersHasMovies;
import com.rashmi.movies.repository.UsersHasTheatersHasMoviesRepository;
import com.rashmi.movies.service.UsersHasTheatersHasMoviesService;
import com.rashmi.movies.service.exception.MovieException;

@Service
@Transactional
public class UsersHasTheatersHasMoviesServiceImpl
        implements UsersHasTheatersHasMoviesService {
    @SuppressWarnings("unused")
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UsersHasTheatersHasMoviesRepository usersHasTheatersHasMoviesRepository;

    @Override
    @Transactional // at method level
    public UsersHasTheatersHasMovies getTxn(long txnId) {
        if (txnId <= 0L) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
                    "txnId is not provided");
        }
        return usersHasTheatersHasMoviesRepository.getTxn(txnId);
    }

    @Override
    @Transactional // at method level
    public GetBuyTicket getTicket(long txnId) {
        if (txnId <= 0L) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
                    "txnId is not provided");
        }
        return usersHasTheatersHasMoviesRepository.getTransaction(txnId);
    }

    @Transactional // at method level
    public GetBuyTicket addTxn(PostBuyTicket newPostBuyTicket) {
        if (newPostBuyTicket == null) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                    "BuyTicket object is not provided in body");
        }

        if (newPostBuyTicket != null) {
            if (StringUtils.isEmpty(newPostBuyTicket.getMovieName())) {
                // TODO what is our exception handling strategy?
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                        "movieName is required");
            }

            if (StringUtils.isEmpty(newPostBuyTicket.getTheaterName())) {
                // TODO what is our exception handling strategy?
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                        "TheaterName is required");
            }

            if (StringUtils.isEmpty(newPostBuyTicket.getEmailId())) {
                // TODO what is our exception handling strategy?
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                        "EmailId is required");
            }

            if (newPostBuyTicket.getNoOfTickets() <= 0) {
                // TODO what is our exception handling strategy?
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                        "NoOfTickets is required and it cannot be 0 or -ve");
            }

            if (StringUtils.isEmpty(newPostBuyTicket.getShowTime())) {
                // TODO what is our exception handling strategy?
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                        "showTime is required");
            }

            if (StringUtils.isEmpty(newPostBuyTicket.getDate())) {
                // TODO what is our exception handling strategy?
                throw new MovieException(
                        com.rashmi.movies.service.exception.ErrorCode.NO_CONTENT,
                        "Date is required");
            }
        }
        GetBuyTicket addedTxn = usersHasTheatersHasMoviesRepository
                .addTxn(newPostBuyTicket);
        return addedTxn;
    }

    @Override
    @Transactional
    public void updateTxn(long txnId, String status) {
        if (txnId <= 0L || StringUtils.isEmpty(status)) {
            throw new MovieException(
                    com.rashmi.movies.service.exception.ErrorCode.MISSING_DATA,
                    "txnId / status is not provided");
        }
        UsersHasTheatersHasMovies usersHasTheatersHasMovies = getTxn(txnId);
        // Setting the status
        usersHasTheatersHasMovies.setStatus(status);
        usersHasTheatersHasMoviesRepository
                .updateTxn(usersHasTheatersHasMovies);
    }
}
