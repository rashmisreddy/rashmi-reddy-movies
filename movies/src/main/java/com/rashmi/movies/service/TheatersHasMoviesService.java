package com.rashmi.movies.service;

import java.util.List;

import com.rashmi.movies.entity.GetShowTime;
import com.rashmi.movies.entity.PostShowTime;
import com.rashmi.movies.entity.TheatersHasMovies;

public interface TheatersHasMoviesService {
    GetShowTime addShowTimes(PostShowTime postShowTime);

    GetShowTime updateShowTimes(PostShowTime putShowTime);

    TheatersHasMovies getTheatersHasMovies(long id);

    TheatersHasMovies getTheatersHasMovies(PostShowTime postShowTime);
    
    List<GetShowTime> getShowTimesByMovieName(String movieName);

    List<GetShowTime> getShowTimesByTheaterName(String theaterName);

    
    // void updateShowTimes(TheatersHasMovies updateTheatersHasMovies);

    // List<GetShowTime> getShowTimesByStatus(String theaterName, String
    // movieName,
    // String status);
    //
    // List<GetShowTime> getShowTimesByTheaterNameMovieName(String theaterName,
    // String movieName);

    // List<GetShowTime> getTheatersHasMoviesByShowTime(String movieName,
    // String theaterName, String showTime, String status);

    // List<GetShowTime> getShowTimesByShowTimeStatus(String theaterName,
    // String movieName, String showTimes, String status);

    // TheatersHasMovies converter(
    // TheatersHasMoviesDelete theatersHasMoviesDelete);
}
