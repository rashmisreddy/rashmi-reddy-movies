-- MySQL Workbench Synchronization
-- Generated: 2015-08-30 02:15
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: rahul

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `movies` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;

CREATE TABLE IF NOT EXISTS `movies`.`Users` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `first_name` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `last_name` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `password` VARCHAR(8) NULL DEFAULT NULL COMMENT '',
  `payment_type` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `emailId` VARCHAR(45) NOT NULL COMMENT '',
  `phone` VARCHAR(15) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`user_id`)  COMMENT '',
  UNIQUE INDEX `idusers_UNIQUE` (`user_id` ASC)  COMMENT '',
  UNIQUE INDEX `emailId_UNIQUE` (`emailId` ASC)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `movies`.`Movies` (
  `movie_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `movie_name` CHAR(50) NULL DEFAULT NULL COMMENT '',
  `movie_details` VARCHAR(1000) NULL DEFAULT NULL COMMENT '',
  `Theaters_theater_id` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`movie_id`, `Theaters_theater_id`)  COMMENT '',
  UNIQUE INDEX `movie_id_UNIQUE` (`movie_id` ASC)  COMMENT '',
  INDEX `fk_Movies_Theaters1_idx` (`Theaters_theater_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Movies_Theaters1`
    FOREIGN KEY (`Theaters_theater_id`)
    REFERENCES `movies`.`Theaters` (`theater_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `movies`.`Theaters` (
  `theater_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `theater_name` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `theater_city` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `theater_state` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `theater_zip` INT(11) NULL DEFAULT NULL COMMENT '',
  `phone` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `aminities` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `movie_price` INT(11) NULL DEFAULT NULL COMMENT '',
  `show_time` DATETIME NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`theater_id`)  COMMENT '',
  UNIQUE INDEX `theater_id_UNIQUE` (`theater_id` ASC)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `movies`.`BuyTickets` (
  `txn_id` VARCHAR(45) NOT NULL COMMENT '',
  `user_id` VARCHAR(45) NOT NULL COMMENT '',
  `number_of_tickets` INT(11) NULL DEFAULT NULL COMMENT '',
  `Users_user_id` INT(11) NOT NULL COMMENT '',
  `Theaters_has_Movies_Theaters_theater_id` INT(11) NOT NULL COMMENT '',
  `Theaters_has_Movies_Movies_movie_id` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`txn_id`, `user_id`, `Users_user_id`, `Theaters_has_Movies_Theaters_theater_id`, `Theaters_has_Movies_Movies_movie_id`)  COMMENT '',
  UNIQUE INDEX `idusers_UNIQUE` (`txn_id` ASC)  COMMENT '',
  INDEX `fk_buy_tickets_Users1_idx` (`Users_user_id` ASC)  COMMENT '',
  CONSTRAINT `fk_buy_tickets_Users1`
    FOREIGN KEY (`Users_user_id`)
    REFERENCES `movies`.`Users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `movies`.`Address` (
  `address_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `street1` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `street2` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `city` VARCHAR(8) NULL DEFAULT NULL COMMENT '',
  `state` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `zip` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `country` VARCHAR(15) NULL DEFAULT NULL COMMENT '',
  `Users_user_id` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`address_id`, `Users_user_id`)  COMMENT '',
  UNIQUE INDEX `idusers_UNIQUE` (`address_id` ASC)  COMMENT '',
  INDEX `fk_Address_Users1_idx` (`Users_user_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Address_Users1`
    FOREIGN KEY (`Users_user_id`)
    REFERENCES `movies`.`Users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

