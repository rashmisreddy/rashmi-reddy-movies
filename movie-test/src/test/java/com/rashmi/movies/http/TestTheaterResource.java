package com.rashmi.movies.http;

import java.util.List;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test error scenarios using a Client
 * http://localhost:8080/theaters/rest/V1.0/theaters.....
 */
public class TestTheaterResource {
    private static final String HTTP_HOST = "http://localhost:8080";
    private static final String URI_PATH = "movies/rest/V1.0/theaters/";

    private Client client = ClientBuilder.newClient();
    private WebTarget target;

    @Before
    public void init() {
        target = client.target(HTTP_HOST).path(URI_PATH);
    }

    @Test
    public void testGetTheaterNoParamsJson() {
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .get();

        // you can use this to print the response
        // System.out.println("HTTP Status=" + response.getStatus());
        // NOTE - you can read the entity ONLY once
        // System.out.println(response.readEntity(String.class));

        verifyMissing(response);
    }

    @Test
    public void testGetTheaterNoParamsXml() {
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .get();

        verifyMissing(response);
    }

    private void verifyMissing(Response response) {
        HttpError error = response.readEntity(HttpError.class);
        Assert.assertEquals(409, response.getStatus());
        Assert.assertEquals(409, error.status);
        //Assert.assertEquals("NO_CONTENT", error.code);
        //Assert.assertEquals("no search parameter provided", error.message);
        Assert.assertEquals("", error.debug);
    }

    @Test
    public void testCreateTheaterNoParamsXml() {
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity("<theater/>", MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreateTheaterNoParamsEntityXml() {
        HttpTheater theaterToSend = new HttpTheater();
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(theaterToSend, MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreateTheaterNoParamsJson() {
        HttpTheater theaterToSend = new HttpTheater();
        
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(theaterToSend,
                        MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreateTheaterNoParamsEntityJson() {
        HttpTheater theaterToSend = new HttpTheater();
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(theaterToSend, MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    private void verifyInvalid(Response response) {
        HttpError error = response.readEntity(HttpError.class);
        Assert.assertEquals(409, response.getStatus());
        Assert.assertEquals(409, error.status);
        //Assert.assertEquals("INVALID_FIELD", error.code);
        Assert.assertEquals("", error.debug);
    }

    @Test
    public void testCreateAndGetTheater() {
        HttpTheater theaterToSend = new HttpTheater();
        long rand = new Random().nextInt(99999);

        theaterToSend.theaterName = "foo" + rand;
        theaterToSend.theaterCity = "city" + rand;
        theaterToSend.theaterState = "state" + rand;
        theaterToSend.theaterZip = rand;
        theaterToSend.theaterPhone = "phone" + rand;
        theaterToSend.theaterAminities = "aminities" + rand;

        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(theaterToSend, MediaType.APPLICATION_XML));

        HttpTheater createResponse = response.readEntity(HttpTheater.class);
        // System.err.println(createResponse);
        Assert.assertEquals(201, response.getStatus());
        Assert.assertNotNull(createResponse.id);
        Assert.assertEquals(createResponse.theaterName,
                theaterToSend.theaterName);
        Assert.assertEquals(createResponse.theaterCity,
                theaterToSend.theaterCity);
        Assert.assertEquals(createResponse.theaterState,
                theaterToSend.theaterState);
        Assert.assertEquals(createResponse.theaterPhone,
                theaterToSend.theaterPhone);
        Assert.assertEquals(createResponse.theaterAminities,
                theaterToSend.theaterAminities);
        Assert.assertNotNull(createResponse.theaterZip);

        // search for just created theater
        Response search = target
                .queryParam("theaterName", theaterToSend.theaterName)
                .queryParam("theaterCity", theaterToSend.theaterCity)
                .queryParam("theaterState", theaterToSend.theaterState)
                .queryParam("theaterPhone", theaterToSend.theaterPhone)
                .queryParam("theaterAminities", theaterToSend.theaterAminities)
                .queryParam("theaterZip", theaterToSend.theaterZip).request()
                .accept(MediaType.APPLICATION_XML).get();
        List<HttpTheater> searchResponse = search
                .readEntity(new GenericType<List<HttpTheater>>() {
                });
        Assert.assertEquals(searchResponse.get(0), createResponse);
    }
}
