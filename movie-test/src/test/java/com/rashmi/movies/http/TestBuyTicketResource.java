package com.rashmi.movies.http;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test error scenarios using a Client
 * http://localhost:8080/movies/rest/V1.0/Ticket.....
 */
public class TestBuyTicketResource {
    private static final String HTTP_HOST = "http://localhost:8080";
    private static final String URI_PATH = "movies/rest/V1.0/Ticket";

    private Client client = ClientBuilder.newClient();
    private WebTarget target;

    @Before
    public void init() {
        target = client.target(HTTP_HOST).path(URI_PATH);
    }

    @Test
    public void testGetBuyTicketNoParamsJson() {
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .accept(MediaType.APPLICATION_XML).get();

        // you can use this to print the response
        System.out.println("HTTP Status=" + response.getStatus());
        // NOTE - you can read the entity ONLY once
        // System.out.println(response.readEntity(String.class));

        verifyMissing(response);
    }

    @Test
    public void testGetBuyTicketNoParamsXml() {
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .accept(MediaType.APPLICATION_XML).get();

        verifyMissing(response);
    }

    private void verifyMissing(Response response) {
        HttpError error = response.readEntity(HttpError.class);
        Assert.assertEquals(409, response.getStatus());
        Assert.assertEquals(409, error.status);
        // Assert.assertEquals("MISSING_DATA", error.code);
        // Assert.assertEquals("no search parameter provided", error.message);
        Assert.assertEquals("", error.debug);
    }

    @Test
    public void testCreatePostBuyTicketNoParamsXml() {
        HttpPostBuyTicket buyTicketToSend = new HttpPostBuyTicket();

        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(buyTicketToSend,
                        MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreatePostBuyTicketNoParamsEntityXml() {
        HttpPostBuyTicket buyTicketToSend = new HttpPostBuyTicket();
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(buyTicketToSend,
                        MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreatePostBuyTicketNoParamsJson() {
        HttpPostBuyTicket buyTicketToSend = new HttpPostBuyTicket();

        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(buyTicketToSend,
                        MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreatePostBuyTicketNoParamsEntityJson() {
        HttpPostBuyTicket buyTicketToSend = new HttpPostBuyTicket();
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(buyTicketToSend,
                        MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    private void verifyInvalid(Response response) {
        HttpError error = response.readEntity(HttpError.class);
        Assert.assertEquals(409, response.getStatus());
        Assert.assertEquals(409, error.status);
        // Assert.assertEquals("INVALID_FIELD", error.code);
        Assert.assertEquals("", error.debug);
    }

    @Test
    public void testCreateAndGetBuyTicket() {
        HttpPostBuyTicket buyTicketToSend = new HttpPostBuyTicket();

        // NOTE: change this data accordingly
        buyTicketToSend.movieName = "foo48107";
        buyTicketToSend.theaterName = "foo33737";
        buyTicketToSend.nooftickets = 1;
        buyTicketToSend.emailId = "fooba31088r@test.com";
        buyTicketToSend.showTime = "20:00:00";
        buyTicketToSend.date = "2015-10-01";

        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(buyTicketToSend,
                        MediaType.APPLICATION_XML));

        HttpGetBuyTicket createResponse = response
                .readEntity(HttpGetBuyTicket.class);
        System.err.println(createResponse);
        Assert.assertEquals(201, response.getStatus());
        Assert.assertNotNull(createResponse.txnid);
        Assert.assertEquals(createResponse.movieName,
                buyTicketToSend.movieName);
        Assert.assertEquals(createResponse.theaterName,
                buyTicketToSend.theaterName);
        Assert.assertEquals(createResponse.emailId, buyTicketToSend.emailId);
        Assert.assertEquals(createResponse.nooftickets,
                buyTicketToSend.nooftickets);
        Assert.assertNotNull(createResponse.totalprice);
        Assert.assertNotNull(createResponse.status);
        // search for just created transaction
        Response search = target.queryParam("txnId", createResponse.txnid)
                .request().accept(MediaType.APPLICATION_XML).get();

        Assert.assertEquals(200, search.getStatus());

        // check this
        // List<HttpGetBuyTicket> searchResponse = search
        // .readEntity(new GenericType<List<HttpGetBuyTicket>>() {
        // });
        // Assert.assertEquals(searchResponse.get(0), createResponse);
    }
}
