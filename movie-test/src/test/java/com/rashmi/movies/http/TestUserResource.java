package com.rashmi.movies.http;

import java.util.List;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test error scenarios using a Client
 * http://localhost:8080/movies/rest/V1.0/users.....
 */
public class TestUserResource {
    private static final String HTTP_HOST = "http://localhost:8080";
    private static final String URI_PATH = "movies/rest/V1.0/users";

    private Client client = ClientBuilder.newClient();
    private WebTarget target;

    @Before
    public void init() {
        target = client.target(HTTP_HOST).path(URI_PATH);
    }

    @Test
    public void testGetUsersNoParamsJson() {
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .get();

        // you can use this to print the response
        // System.out.println("HTTP Status=" + response.getStatus());
        // NOTE - you can read the entity ONLY once
        // System.out.println(response.readEntity(String.class));

        verifyMissing(response);
    }

    @Test
    public void testGetUsersNoParamsXml() {
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .get();

        verifyMissing(response);
    }

    private void verifyMissing(Response response) {
        HttpError error = response.readEntity(HttpError.class);
        Assert.assertEquals(409, response.getStatus());
        Assert.assertEquals(409, error.status);
        // Assert.assertEquals("MISSING_DATA", error.code);
        // Assert.assertEquals("no search parameter provided", error.message);
        Assert.assertEquals("", error.debug);
    }

    @Test
    public void testCreateUsersNoParamsXml() {
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity("<user/>", MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreateUsersNoParamsEntityXml() {
        HttpUser userToSend = new HttpUser();
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(userToSend, MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreateUsersNoParamsJson() {
        HttpUser userToSend = new HttpUser();

        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(userToSend, MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreateUsersNoParamsEntityJson() {
        HttpUser userToSend = new HttpUser();
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(userToSend, MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    private void verifyInvalid(Response response) {
        HttpError error = response.readEntity(HttpError.class);
        Assert.assertEquals(409, response.getStatus());
        Assert.assertEquals(409, error.status);
        Assert.assertEquals("NO_CONTENT", error.code);
        // Assert.assertEquals("password is required", error.message);
        Assert.assertEquals("", error.debug);
    }

    @Test
    public void testCreateAndGetUser() {
        HttpUser userToSend = new HttpUser();
        long rand = new Random().nextInt(99999);

        HttpAddress addressToSend = new HttpAddress();

        addressToSend.street1 = "street1";
        addressToSend.street2 = "street2";
        addressToSend.city = "city";
        addressToSend.state = "state";
        addressToSend.zip = 12345;
        addressToSend.country = "USA";

        userToSend.firstName = "foo" + rand;
        userToSend.lastName = "bar" + rand;
        userToSend.password = "pass123";
        userToSend.emailId = "foobar" + rand + "@test.com";
        userToSend.paymentType = "credit card";
        userToSend.phone = 123456789;
        userToSend.newAddress = addressToSend;

        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(userToSend, MediaType.APPLICATION_XML));

        HttpUser createResponse = response.readEntity(HttpUser.class);
        // System.err.println(createResponse);
        Assert.assertEquals(201, response.getStatus());
        Assert.assertNotNull(createResponse.id);
        Assert.assertEquals(createResponse.firstName, userToSend.firstName);
        Assert.assertEquals(createResponse.lastName, userToSend.lastName);
        Assert.assertNotNull(createResponse.password);
        Assert.assertEquals(createResponse.emailId, userToSend.emailId);
        Assert.assertEquals(createResponse.paymentType, userToSend.paymentType);
        Assert.assertEquals(createResponse.phone, userToSend.phone);

        // search for just created user
        Response search = target.queryParam("firstName", userToSend.firstName)
                .queryParam("lastName", userToSend.lastName).request()
                .accept(MediaType.APPLICATION_XML).get();
        List<HttpUser> searchResponse = search
                .readEntity(new GenericType<List<HttpUser>>() {
                });
        Assert.assertEquals(searchResponse.get(0), createResponse);
    }
}
