package com.rashmi.movies.http;

import java.util.List;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test error scenarios using a Client
 * http://localhost:8080/movies/rest/V1.0/movies.....
 */
public class TestMovieResource {
    private static final String HTTP_HOST = "http://localhost:8080";
    private static final String URI_PATH = "movies/rest/V1.0/movies";

    private Client client = ClientBuilder.newClient();
    private WebTarget target;

    @Before
    public void init() {
        target = client.target(HTTP_HOST).path(URI_PATH);
    }

    @Test
    public void testGetMovieNoParamsJson() {
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .accept(MediaType.APPLICATION_XML).get();

        // you can use this to print the response
        // System.out.println("HTTP Status=" + response.getStatus());
        // NOTE - you can read the entity ONLY once
        // System.out.println(response.readEntity(String.class));

        verifyMissing(response);
    }

    @Test
    public void testGetMovieNoParamsXml() {
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .accept(MediaType.APPLICATION_XML).get();

        verifyMissing(response);
    }

    private void verifyMissing(Response response) {
        HttpError error = response.readEntity(HttpError.class);
        Assert.assertEquals(409, response.getStatus());
        Assert.assertEquals(409, error.status);
        Assert.assertEquals("MISSING_DATA", error.code);
        // Assert.assertEquals("no search parameter provided", error.message);
        Assert.assertEquals("", error.debug);
    }

    @Test
    public void testCreateMovieNoParamsXml() {
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity("<movie/>", MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreateMovieNoParamsEntityXml() {
        HttpMovie movieToSend = new HttpMovie();
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(movieToSend, MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreateMovieNoParamsJson() {
        HttpMovie movieToSend = new HttpMovie();

        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(movieToSend, MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreateMovieNoParamsEntityJson() {
        HttpMovie movieToSend = new HttpMovie();
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(movieToSend, MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    private void verifyInvalid(Response response) {
        HttpError error = response.readEntity(HttpError.class);
        Assert.assertEquals(409, response.getStatus());
        Assert.assertEquals(409, error.status);
        // Assert.assertEquals("INVALID_FIELD", error.code);
        // Assert.assertEquals("no search parameter provided", error.message);
        Assert.assertEquals("", error.debug);
    }

    @Test
    public void testCreateAndGetMovie() {
        HttpMovie movieToSend = new HttpMovie();
        long rand = new Random().nextInt(99999);

        movieToSend.movieName = "foo" + rand;
        movieToSend.movieDetails = "details" + rand;

        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(movieToSend, MediaType.APPLICATION_XML));

        HttpMovie createResponse = response.readEntity(HttpMovie.class);
        // System.err.println(createResponse);
        Assert.assertEquals(201, response.getStatus());
        Assert.assertNotNull(createResponse.id);
        Assert.assertEquals(createResponse.movieName, movieToSend.movieName);
        Assert.assertEquals(createResponse.movieDetails,
                movieToSend.movieDetails);

        // search for just created movie
        Response search = target.queryParam("movieName", movieToSend.movieName)
                .queryParam("movieDetails", movieToSend.movieDetails).request()
                .accept(MediaType.APPLICATION_XML).get();
        List<HttpMovie> searchResponse = search
                .readEntity(new GenericType<List<HttpMovie>>() {
                });
        Assert.assertEquals(searchResponse.get(0), createResponse);
    }
}
