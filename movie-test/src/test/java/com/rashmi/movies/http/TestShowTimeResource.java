package com.rashmi.movies.http;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test error scenarios using a Client
 * http://localhost:8080/movies/rest/V1.0/showtimes.....
 */
public class TestShowTimeResource {
    private static final String HTTP_HOST = "http://localhost:8080";
    private static final String URI_PATH = "movies/rest/V1.0/showtimes";

    private Client client = ClientBuilder.newClient();
    private WebTarget target;

    @Before
    public void init() {
        target = client.target(HTTP_HOST).path(URI_PATH);
    }

    @Test
    public void testGetShowTimeNoParamsJson() {
        Response response = target.path("/byTheaterName").request()
                .accept(MediaType.APPLICATION_XML)
                .accept(MediaType.APPLICATION_XML).get();

        // you can use this to print the response
        // System.out.println("HTTP Status=" + response.getStatus());
        // NOTE - you can read the entity ONLY once
        // System.out.println(response.readEntity(String.class));

        verifyMissing(response);
    }

    @Test
    public void testGetShowTimeNoParamsXml() {
        Response response = target.path("/byTheaterName").request()
                .accept(MediaType.APPLICATION_XML)
                .accept(MediaType.APPLICATION_XML).get();

        verifyMissing(response);
    }

    private void verifyMissing(Response response) {
        HttpError error = response.readEntity(HttpError.class);
        Assert.assertEquals(409, response.getStatus());
        Assert.assertEquals(409, error.status);
        // Assert.assertEquals("MISSING_DATA", error.code);
        // Assert.assertEquals("no search parameter provided", error.message);
        Assert.assertEquals("", error.debug);
    }

    @Test
    public void testCreateShowTimeNoParamsXml() {
        HttpPostShowTime showTimeToSend = new HttpPostShowTime();

        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(showTimeToSend, MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreateShowTimeNoParamsEntityXml() {
        HttpPostShowTime showTimeToSend = new HttpPostShowTime();
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(showTimeToSend, MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreateShowTimeNoParamsJson() {
        HttpPostShowTime showTimeToSend = new HttpPostShowTime();

        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(showTimeToSend, MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    @Test
    public void testCreateShowTimeNoParamsEntityJson() {
        HttpPostShowTime showTimeToSend = new HttpPostShowTime();
        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(showTimeToSend, MediaType.APPLICATION_XML));

        verifyInvalid(response);
    }

    private void verifyInvalid(Response response) {
        HttpError error = response.readEntity(HttpError.class);
        Assert.assertEquals(409, response.getStatus());
        Assert.assertEquals(409, error.status);
        // Assert.assertEquals("INVALID_FIELD", error.code);
        Assert.assertEquals("", error.debug);
    }

    @Test
    public void testCreateAndUpdateShowTime() {
        HttpPostShowTime showTimeToSend = new HttpPostShowTime();
        // long rand = new Random().nextInt(99999);

        showTimeToSend.movieName = "foo30572";
        showTimeToSend.theaterName = "foo29218";
        showTimeToSend.fromDate = "2015-10-01";
        showTimeToSend.toDate = "2015-11-01";
        showTimeToSend.ShowTime = "20:00:00";
        showTimeToSend.price = 5;
        showTimeToSend.status = "ACTIVE";

        Response response = target.request().accept(MediaType.APPLICATION_XML)
                .post(Entity.entity(showTimeToSend, MediaType.APPLICATION_XML));

        HttpGetShowTime createResponse = response
                .readEntity(HttpGetShowTime.class);
        // System.err.println(createResponse);
        Assert.assertEquals(201, response.getStatus());
        Assert.assertNotNull(createResponse.id);
        Assert.assertEquals(createResponse.movieName, showTimeToSend.movieName);
        Assert.assertEquals(createResponse.theaterName,
                showTimeToSend.theaterName);
        Assert.assertEquals(createResponse.ShowTime, showTimeToSend.ShowTime);
        // Assert.assertEquals(createResponse.fromDate +" 00:00:00",
        // showTimeToSend.fromDate);
        // Assert.assertEquals(createResponse.toDate + " 00:00:00",
        // showTimeToSend.toDate);
        Assert.assertEquals(createResponse.price, showTimeToSend.price);
        Assert.assertEquals(createResponse.status, showTimeToSend.status);

        // We should make the status as "RUNNING" and then this code will work
        Response updateSearch = target.path("/update/").request()
                .accept(MediaType.APPLICATION_XML)
                .put(Entity.entity(createResponse, MediaType.APPLICATION_XML));
    }

    @Test
    public void getShowTime() {
        String movieName = "foo30572";

        Response search = target.path("/byMovieName")
                .queryParam("movieName", movieName).request()
                .accept(MediaType.APPLICATION_XML).get();

        Assert.assertEquals(200, search.getStatus());

        List<HttpGetShowTime> searchResponse = search
                .readEntity(new GenericType<List<HttpGetShowTime>>() {
                });
        // Assert.assertEquals(searchResponse.get(0).movieName, movieName);
    }
}
