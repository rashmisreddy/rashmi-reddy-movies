package com.rashmi.movies.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "buyTicket")
public class HttpPostBuyTicket {
    @XmlElement
    public String movieName;

    @XmlElement
    public String theaterName;

    @XmlElement
    public String emailId;

    @XmlElement
    public long nooftickets;

    @XmlElement
    public String showTime;

    @XmlElement
    public String date;

    @Override
    public String toString() {
        return "HttpPostBuyTicket [movieName=" + movieName + ", theaterName="
                + theaterName + ", emailId=" + emailId + ", nooftickets="
                + nooftickets + "showTime=" + "showTime" + "date=" + "date"
                + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result
                + ((movieName == null) ? 0 : movieName.hashCode());

        result = prime * result
                + ((theaterName == null) ? 0 : theaterName.hashCode());

        result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());

        result = prime * result + (int) (nooftickets ^ (nooftickets >>> 32));

        result = prime * result
                + ((showTime == null) ? 0 : showTime.hashCode());

        result = prime * result + ((date == null) ? 0 : date.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HttpPostBuyTicket other = (HttpPostBuyTicket) obj;

        if (theaterName == null) {
            if (other.theaterName != null)
                return false;
        } else if (!theaterName.equals(other.theaterName))
            return false;

        if (movieName == null) {
            if (other.movieName != null)
                return false;
        } else if (!movieName.equals(other.movieName))
            return false;

        if (emailId == null) {
            if (other.emailId != null)
                return false;
        } else if (!emailId.equals(other.emailId))
            return false;

        if (nooftickets != other.nooftickets)
            return false;

        if (showTime == null) {
            if (other.showTime != null)
                return false;
        } else if (!showTime.equals(other.showTime))
            return false;

        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;

        return true;
    }
}
