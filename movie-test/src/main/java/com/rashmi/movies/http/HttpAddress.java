package com.rashmi.movies.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "address")
public class HttpAddress {
    @XmlElement
    public long id;

    @XmlElement
    public String street1;

    @XmlElement
    public String street2;

    @XmlElement
    public String city;

    @XmlElement
    public String state;

    @XmlElement
    public long zip;

    @XmlElement
    public String country;

    @Override
    public String toString() {
        return "HttpUser [id=" + id + ", street1=" + street1 + ", street2="
                + street2 + ", city=" + city + ", state=" + state + ", zip="
                + zip + ", country=" + country + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((street1 == null) ? 0 : street1.hashCode());

        result = prime * result + (int) (id ^ (id >>> 32));

        result = prime * result + ((street2 == null) ? 0 : street2.hashCode());

        result = prime * result + ((city == null) ? 0 : city.hashCode());

        result = prime * result + (int) (zip ^ (zip >>> 32));

        result = prime * result + ((state == null) ? 0 : state.hashCode());

        result = prime * result + ((country == null) ? 0 : country.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HttpAddress other = (HttpAddress) obj;

        if (street1 == null) {
            if (other.street1 != null)
                return false;
        } else if (!street1.equals(other.street1))
            return false;

        if (id != other.id)
            return false;

        if (zip != other.zip)
            return false;

        if (street2 == null) {
            if (other.street2 != null)
                return false;
        } else if (!street2.equals(other.street2))
            return false;

        if (city == null) {
            if (other.city != null)
                return false;
        } else if (!city.equals(other.city))
            return false;

        if (state == null) {
            if (other.state != null)
                return false;
        } else if (!state.equals(other.state))
            return false;

        if (country == null) {
            if (other.country != null)
                return false;
        } else if (!country.equals(other.country))
            return false;

        return true;
    }
}
