package com.rashmi.movies.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "showtime")
public class HttpPostShowTime {
    @XmlElement(name = "movieName")
    public String movieName;

    @XmlElement(name = "theaterName")
    public String theaterName;

    @XmlElement(name = "ShowTime")
    public String ShowTime;

    @XmlElement(name = "price")
    public long price;

    @XmlElement(name = "status")
    public String status;

    @XmlElement(name = "fromDate")
    public String fromDate;

    @XmlElement(name = "toDate")
    public String toDate;

    @Override
    public String toString() {
        return "HttpGetShowTime [movieName=" + movieName + ", theaterName="
                + theaterName + ", ShowTime=" + ShowTime + ", price=" + price
                + ", status=" + status + ", fromDate=" + fromDate + ", toDate="
                + toDate + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result
                + ((theaterName == null) ? 0 : theaterName.hashCode());

        result = prime * result
                + ((movieName == null) ? 0 : movieName.hashCode());

        result = prime * result
                + ((ShowTime == null) ? 0 : ShowTime.hashCode());

        result = prime * result + (int) (price ^ (price >>> 32));

        result = prime * result
                + ((fromDate == null) ? 0 : fromDate.hashCode());

        result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());

        result = prime * result + ((status == null) ? 0 : status.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HttpPostShowTime other = (HttpPostShowTime) obj;

        if (ShowTime == null) {
            if (other.ShowTime != null)
                return false;
        } else if (!ShowTime.equals(other.ShowTime))
            return false;

        if (theaterName == null) {
            if (other.theaterName != null)
                return false;
        } else if (!theaterName.equals(other.theaterName))
            return false;

        if (movieName == null) {
            if (other.movieName != null)
                return false;
        } else if (!movieName.equals(other.movieName))
            return false;

        if (price != other.price)
            return false;

        if (fromDate == null) {
            if (other.fromDate != null)
                return false;
        } else if (!fromDate.equals(other.fromDate))
            return false;

        if (toDate == null) {
            if (other.toDate != null)
                return false;
        } else if (!toDate.equals(other.toDate))
            return false;

        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;

        return true;
    }
}
