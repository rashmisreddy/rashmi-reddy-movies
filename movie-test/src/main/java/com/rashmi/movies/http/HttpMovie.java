package com.rashmi.movies.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "movie")
public class HttpMovie {
    @XmlElement
    public long id;

    @XmlElement
    public String movieName;

    @XmlElement
    public String movieDetails;

    @Override
    public String toString() {
        return "HttpMovie [id=" + id + ", movieName=" + movieName
                + ", movieDetails=" + movieDetails + "]";
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result
                + ((movieName == null) ? 0 : movieName.hashCode());

        result = prime * result
                + ((movieDetails == null) ? 0 : movieDetails.hashCode());
        
        result = prime * result + (int) (id ^ (id >>> 32));

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HttpMovie other = (HttpMovie) obj;

        if (movieName == null) {
            if (other.movieName != null)
                return false;
        } else if (!movieName.equals(other.movieName))
            return false;

        if (id != other.id)
            return false;

        return true;
    }
}
