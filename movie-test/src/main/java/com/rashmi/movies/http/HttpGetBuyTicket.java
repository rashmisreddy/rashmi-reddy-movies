package com.rashmi.movies.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "buyTicket")
public class HttpGetBuyTicket {
    @XmlElement
    public long txnid;

    @XmlElement
    public String movieName;

    @XmlElement
    public String theaterName;

    @XmlElement
    public String emailId;

    @XmlElement
    public long nooftickets;

    @XmlElement
    public long totalprice;

    @XmlElement
    public String status;

    @XmlElement
    public String date;

    @Override
    public String toString() {
        return "HttpGetBuyTicket [txnid=" + txnid + ", movieName=" + movieName
                + ", theaterName=" + theaterName + ", emailId=" + emailId
                + ", nooftickets=" + nooftickets + ", totalprice=" + totalprice
                + ", status=" + status + ", date=" + "date" + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result
                + ((movieName == null) ? 0 : movieName.hashCode());

        result = prime * result
                + ((theaterName == null) ? 0 : theaterName.hashCode());

        result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());

        result = prime * result + ((status == null) ? 0 : status.hashCode());

        result = prime * result + (int) (txnid ^ (txnid >>> 32));

        result = prime * result + (int) (nooftickets ^ (nooftickets >>> 32));

        result = prime * result + (int) (totalprice ^ (totalprice >>> 32));

        result = prime * result + ((date == null) ? 0 : date.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HttpGetBuyTicket other = (HttpGetBuyTicket) obj;

        if (theaterName == null) {
            if (other.theaterName != null)
                return false;
        } else if (!theaterName.equals(other.theaterName))
            return false;

        if (movieName == null) {
            if (other.movieName != null)
                return false;
        } else if (!movieName.equals(other.movieName))
            return false;

        if (emailId == null) {
            if (other.emailId != null)
                return false;
        } else if (!emailId.equals(other.emailId))
            return false;

        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;

        if (txnid != other.txnid)
            return false;

        if (nooftickets != other.nooftickets)
            return false;

        if (totalprice != other.totalprice)
            return false;

        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;

        return true;
    }
}
