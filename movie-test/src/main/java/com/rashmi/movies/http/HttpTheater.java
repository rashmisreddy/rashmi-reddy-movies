package com.rashmi.movies.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "theater")
public class HttpTheater {
    @XmlElement
    public long id;

    @XmlElement
    public String theaterName;

    @XmlElement
    public String theaterCity;

    @XmlElement
    public String theaterState;

    @XmlElement
    public long theaterZip;

    @XmlElement
    public String theaterPhone;

    @XmlElement
    public String theaterAminities;

    @Override
    public String toString() {
        return "HttpTheater [id=" + id + ", theaterName=" + theaterName
                + ", theaterCity=" + theaterCity + ", theaterState="
                + theaterState + ", theaterZip=" + theaterZip
                + ", theaterPhone=" + theaterPhone + ", theaterAminities="
                + theaterAminities + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result
                + ((theaterName == null) ? 0 : theaterName.hashCode());

        result = prime * result
                + ((theaterCity == null) ? 0 : theaterCity.hashCode());

        result = prime * result
                + ((theaterState == null) ? 0 : theaterState.hashCode());

        result = prime * result + ((theaterAminities == null) ? 0
                : theaterAminities.hashCode());

        result = prime * result
                + ((theaterPhone == null) ? 0 : theaterPhone.hashCode());

        result = prime * result + (int) (id ^ (id >>> 32));
        
        result = prime * result + (int) (theaterZip ^ (theaterZip >>> 32));

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HttpTheater other = (HttpTheater) obj;

        if (theaterName == null) {
            if (other.theaterName != null)
                return false;
        } else if (!theaterName.equals(other.theaterName))
            return false;

        if (theaterCity == null) {
            if (other.theaterCity != null)
                return false;
        } else if (!theaterCity.equals(other.theaterCity))
            return false;
        
        if (theaterState == null) {
            if (other.theaterState != null)
                return false;
        } else if (!theaterState.equals(other.theaterState))
            return false;
        
        if (theaterPhone == null) {
            if (other.theaterPhone != null)
                return false;
        } else if (!theaterPhone.equals(other.theaterPhone))
            return false;
        
        if (theaterAminities == null) {
            if (other.theaterAminities != null)
                return false;
        } else if (!theaterAminities.equals(other.theaterAminities))
            return false;
        
        if (id != other.id)
            return false;

        if (theaterZip != other.theaterZip)
            return false;
        
        return true;
    }
}
