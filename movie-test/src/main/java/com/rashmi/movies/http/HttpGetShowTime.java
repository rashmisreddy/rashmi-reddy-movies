package com.rashmi.movies.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "showtime")
public class HttpGetShowTime {

    @XmlElement
    public long id;

    @XmlElement
    public String movieName;

    @XmlElement
    public String theaterName;

    @XmlElement
    public String theaterCity;

    @XmlElement
    public String theaterState;

    @XmlElement
    public long theaterZip;

    @XmlElement
    public String theaterPhone;

    @XmlElement
    public String theaterAminities;

    @XmlElement
    public String ShowTime;

    @XmlElement
    public long price;

    @XmlElement
    public String movieDetails;

    @XmlElement(name = "status")
    public String status;

    @XmlElement(name = "fromDate")
    public String fromDate;

    @XmlElement(name = "toDate")
    public String toDate;

    @Override
    public String toString() {
        return "HttpGetShowTime [id=" + id + ", movieName=" + movieName
                + ", movieDetails=" + movieDetails + ", theaterName="
                + theaterName + ", theaterCity=" + theaterCity
                + ", theaterState=" + theaterState + ", theaterZip="
                + theaterZip + ", theaterPhone=" + theaterPhone
                + ", theaterAminities=" + theaterAminities + ", ShowTime="
                + ShowTime + ", price=" + price + ", status=" + status
                + ", fromDate=" + fromDate + ", toDate=" + toDate + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result
                + ((theaterName == null) ? 0 : theaterName.hashCode());

        result = prime * result
                + ((movieName == null) ? 0 : movieName.hashCode());

        result = prime * result
                + ((theaterCity == null) ? 0 : theaterCity.hashCode());

        result = prime * result
                + ((theaterState == null) ? 0 : theaterState.hashCode());

        result = prime * result + ((theaterAminities == null) ? 0
                : theaterAminities.hashCode());

        result = prime * result
                + ((theaterPhone == null) ? 0 : theaterPhone.hashCode());

        result = prime * result
                + ((ShowTime == null) ? 0 : ShowTime.hashCode());

        result = prime * result + (int) (id ^ (id >>> 32));

        result = prime * result + (int) (theaterZip ^ (theaterZip >>> 32));

        result = prime * result + (int) (price ^ (price >>> 32));

        result = prime * result
                + ((movieDetails == null) ? 0 : movieDetails.hashCode());

        result = prime * result + ((status == null) ? 0 : status.hashCode());

        result = prime * result
                + ((fromDate == null) ? 0 : fromDate.hashCode());

        result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HttpGetShowTime other = (HttpGetShowTime) obj;

        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;

        if (ShowTime == null) {
            if (other.ShowTime != null)
                return false;
        } else if (!ShowTime.equals(other.ShowTime))
            return false;

        if (theaterName == null) {
            if (other.theaterName != null)
                return false;
        } else if (!theaterName.equals(other.theaterName))
            return false;

        if (movieName == null) {
            if (other.movieName != null)
                return false;
        } else if (!movieName.equals(other.movieName))
            return false;

        if (theaterCity == null) {
            if (other.theaterCity != null)
                return false;
        } else if (!theaterCity.equals(other.theaterCity))
            return false;

        if (theaterState == null) {
            if (other.theaterState != null)
                return false;
        } else if (!theaterState.equals(other.theaterState))
            return false;

        if (theaterPhone == null) {
            if (other.theaterPhone != null)
                return false;
        } else if (!theaterPhone.equals(other.theaterPhone))
            return false;

        if (theaterAminities == null) {
            if (other.theaterAminities != null)
                return false;
        } else if (!theaterAminities.equals(other.theaterAminities))
            return false;

        if (movieDetails == null) {
            if (other.movieDetails != null)
                return false;
        } else if (!movieDetails.equals(other.movieDetails))
            return false;

        if (id != other.id)
            return false;

        if (theaterZip != other.theaterZip)
            return false;

        if (price != other.price)
            return false;

        if (fromDate == null) {
            if (other.fromDate != null)
                return false;
        } else if (!fromDate.equals(other.fromDate))
            return false;

        if (toDate == null) {
            if (other.toDate != null)
                return false;
        } else if (!toDate.equals(other.toDate))
            return false;

        return true;
    }
}
