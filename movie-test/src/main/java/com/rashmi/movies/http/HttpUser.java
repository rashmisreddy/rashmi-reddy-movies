package com.rashmi.movies.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Select fields we want exposed to the REST layer. Separation from
 * business/data layer.
 * 
 * Note "XML" annotation. The resteasy-jettison implementation converts these to
 * JSON depending on the Accept media type
 * 
 *
 */
@XmlRootElement(name = "user")
public class HttpUser {

    @XmlElement
    public long id;

    @XmlElement
    public String firstName;

    @XmlElement
    public String lastName;

    @XmlElement
    public String password;

    @XmlElement
    public String paymentType;

    @XmlElement
    public String emailId;

    @XmlElement
    public long phone;

    @XmlElement(name = "address", required = true, type = HttpAddress.class)
    public HttpAddress newAddress;

    @Override
    public String toString() {
        return "HttpUser [id=" + id + ", firstName=" + firstName + ", lastName="
                + lastName + ", password=" + password + ", paymentType="
                + paymentType + ", emailId=" + emailId + ", phone=" + phone
                + ", newAddress=" + newAddress + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result
                + ((firstName == null) ? 0 : firstName.hashCode());

        result = prime * result + (int) (id ^ (id >>> 32));

        result = prime * result
                + ((lastName == null) ? 0 : lastName.hashCode());

        result = prime * result
                + ((password == null) ? 0 : password.hashCode());

        result = prime * result + (int) (phone ^ (phone >>> 32));

        result = prime * result
                + ((paymentType == null) ? 0 : paymentType.hashCode());

        result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());

        result = prime * result
                + ((newAddress == null) ? 0 : newAddress.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HttpUser other = (HttpUser) obj;

        if (firstName == null) {
            if (other.firstName != null)
                return false;
        } else if (!firstName.equals(other.firstName))
            return false;

        if (id != other.id)
            return false;

        if (phone != other.phone)
            return false;

        if (lastName == null) {
            if (other.lastName != null)
                return false;
        } else if (!lastName.equals(other.lastName))
            return false;

        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;

        if (emailId == null) {
            if (other.emailId != null)
                return false;
        } else if (!emailId.equals(other.emailId))
            return false;

        if (paymentType == null) {
            if (other.paymentType != null)
                return false;
        } else if (!paymentType.equals(other.paymentType))
            return false;

        if (newAddress == null) {
            if (other.newAddress != null)
                return false;
        } else if (!newAddress.equals(other.newAddress))
            return false;
        return true;
    }
}
